<?php
/**
 * Update Wp Job Manager Options
 */

add_action( 'after_switch_theme', 'prolist_after_switch_theme' );

function prolist_after_switch_theme(){
    update_option( 'job_manager_paid_listings_flow', 'before' );
    update_option( 'job_manager_enable_tag_archive', '1' );
    update_option( 'job_manager_tag_input', 'multiselect' );
    update_option( 'job_manager_enable_categories', '1' );
    update_option( 'job_manager_enable_default_category_multiselect', '1' );
    update_option( 'job_manager_regions_filter', '1' );
}

// Load the TGM init if it exists
if ( file_exists( dirname( __FILE__ ) . '/tgm/tgm-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/tgm/tgm-init.php';
}
// Load the library for Importer
if ( file_exists( dirname( __FILE__ ) . '/importer-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/importer-init.php';
}
// Load the Importer if it exists
if ( file_exists( dirname( __FILE__ ) . '/importer.php' ) ) {
    require_once dirname( __FILE__ ) . '/importer.php';
}