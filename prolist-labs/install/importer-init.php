<?php
/**
 * Version 1.0.0
 *
 */
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Don't duplicate me!
if ( !class_exists( 'prolist_Theme_Demo_Data_Importer' ) ) {

	require_once( dirname( __FILE__ ) . '/lib/prolist-importer.php' ); //load admin theme data importer

	class prolist_Theme_Demo_Data_Importer extends prolist_Importer {

		/**
		 * Holds a copy of the object for easy reference.
		 *
		 * @since 0.0.1
		 *
		 * @var object
		 */
		private static $instance;

		/**
		 * Set name of the theme options file
		 *
		 * @since 0.0.2
		 *
		 * @var string
		 */
		public $theme_options_file_name = 'theme_options.txt';

		public $wp_options_file_name = 'wp_options.json';

		public $customizer_file_name = 'customizer.json';

		public $ess_file_name = 'ess_grid.json';

		public $rev_file_name = 'rev';

		/**
		 * Set name of the widgets json file
		 *
		 * @since 0.0.2
		 *
		 * @var string
		 */
		public $widgets_file_name       = 'widgets.json';

		/**
		 * Set name of the content file
		 *
		 * @since 0.0.2
		 *
		 * @var string
		 */
		public $content_demo_file_name  = 'content.xml';

		/**
		 * Holds a copy of the widget settings
		 *
		 * @since 0.0.2
		 *
		 * @var string
		 */
		public $widget_import_results;

		public $demo_files_path;

		/**
		 * Constructor. Hooks all interactions to initialize the class.
		 *
		 * @since 0.0.1
		 */
		public function __construct() {
			$this->demo_files_path = dirname(__FILE__) . '/demo/prolist_01/'; //can
			self::$instance = $this;
			parent::__construct();
		}

		/**
		 * Add menus - the menus listed here largely depend on the ones registered in the theme
		 *
		 * @since 0.0.1
		 */
		public function set_demo_menus(){

			// Menus to Import and assign - you can remove or add as many as you want
			$primary   = get_term_by('name', 'Primary', 'nav_menu');
			$user = get_term_by('name', 'User', 'nav_menu');
			$footer = get_term_by('name', 'Footer', 'nav_menu');

			if ( isset( $primary->term_id ) ) {
				set_theme_mod( 'nav_menu_locations', array(
						'primary' 	=> $primary->term_id,
						'user' 		=> $user->term_id,
						'footer' 	=> $footer->term_id,
					)
				);
			}
			$this->flag_as_imported['menus'] = true;

		}

	}

}