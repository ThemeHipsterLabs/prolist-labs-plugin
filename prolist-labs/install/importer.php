<?php
/**
 * ProList - Stunning Wordpress Theme functions and definitions
 *
 * @package ProList - Stunning Wordpress Theme
 */

// Add Menu
add_action( 'admin_menu', 'prolist_register_importer_menu_page' );
function prolist_register_importer_menu_page(){
	add_menu_page( 'ProList', esc_html__( 'ProList', 'prolist'), 'manage_options', 'prolist_welcome','prolist_dashboard_page', get_template_directory_uri().'/assets/admin/img/logo-menu.png', 61 );
}
function prolist_dashboard_page(){
	echo '<h1>Here, ProList Theme.</h1>';
}
// Add Sub Menu
add_action( 'admin_menu', 'prolist_sub_menu_importer' );
function prolist_sub_menu_importer() {
	add_submenu_page( 'prolist_welcome', esc_html__( 'Data Importer', 'prolist'), 'Importer', 'edit_theme_options', 'prolist_importer', 'prolist_importer_page', null, 1 );
}
// Dashboard
function prolist_importer_page(){
	$nonce = wp_create_nonce("prolist_import_nonce");
	?>
	<div class="wrap">
	    <h1 class="page-title"><?php _e( 'Import Demo Data', 'prolist') ?></h1>
	    <div id="welcome-panel" class="welcome-panel" style="padding: 20px;">
			<p class="tie_message_hint"><?php _e('Importing demo data (post, pages, images, theme settings, ...) is the easiest way to setup your theme. It will
		allow you to quickly edit everything instead of creating content from scratch. When you import the data following things will happen:','prolist');?></p>

			<ul style="list-style-position: inside;list-style-type: square;}">
			  	<li><?php _e( 'Works best to import on a new install of WordPress','prolist');?></li>
			  	<li><?php _e( 'No existing posts, pages, categories, images, custom post types or any other data will be deleted or modified.','prolist');?></li>
			  	<li><?php _e( 'No WordPress settings will be modified.','prolist');?></li>
			  	<li><?php _e( 'Posts, pages, some images, some widgets and menus will get imported.','prolist');?></li>
			  	<li><?php _e( 'Images will be downloaded from our server, these images are copyrighted and are for demo use only.','prolist');?></li>
			  	<li><?php _e( 'Please click import only once and wait, it can take a couple of minutes','prolist');?></li>
			</ul>
		</div>
	    <form method="post" action="" id="importContentForm">
	        <table class="form-table">
	            <tr>
	            	<td>
	            	    <p class="submit">
	            	        <input type="submit" name="submit" id="start_import" class="button button-primary" value="<?php _e( 'Start Import', 'prolist') ?>">
	            	        <span class="spinner" id="import_spinner" style="float: none;"></span>
	            	        <input type="hidden" name="nonce" id="nonce" value="<?php echo $nonce;?>">
	            	    </p>
					</td>
				</tr>
	        </table>
	    </form>
	</div>
	<?php
}
// Insert Javascript
add_action( 'admin_footer', 'prolist_import_javascript' ); // Write our JS below here

function prolist_import_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery("#start_import").live('click', function() {
			var demo_package = jQuery('#demo_package').val();
			var nonce = jQuery('#nonce').val();
			if (confirm('Do you want to import demo now?')) {
				jQuery(this).prop('disabled', true).next().addClass('is-active');
				jQuery.ajax({
					type 		: "post",
					dataType 	: "json",
					url 		: ajaxurl,
					data 		: {
						action: "prolist_import",
						nonce: nonce
					},
					success: function(getData) {
					    if(getData.type == "success") {
					    	jQuery("#start_import").prop('disabled', true).val( 'Imported' ).next().removeClass('is-active');
					    	jQuery("#start_import").before('<div class="updated notice is-dismissible rl-notice"><p>All Done. Have fun! Remember to Save Permalink, update the passwords and roles of imported users.</p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>');
					    }
					    else {
					       	alert(getData.message);
					    }
					}
				})
			}
			return false;
		});
	});
	</script> <?php
}
// Action for Ajax
add_action( 'wp_ajax_prolist_import', 'prolist_import_callback' );
function prolist_import_callback() {
	// Sercurity
	if ( !wp_verify_nonce( $_REQUEST['nonce'], "prolist_import_nonce")) {
		exit("No naughty business please");
	}
	// Process
		ob_start();
	    $message = '';
		$prolist_import = new prolist_Theme_Demo_Data_Importer();
		$prolist_import->process_imports();
		$flag = $prolist_import->flag_as_imported;
		ob_get_clean();
		if ($flag['content'] == true && $flag['menus'] == true && $flag['widgets'] == true) {
			$result['type'] = "success";
			$result['message'] = '';
		}else{
			if ($flag['content'] == false) {
				$message .= __('Error on Content import.','prolist');
			}
			if ($flag['menus'] == false) {
				$message .= __('Error on Menus import.','prolist');
			}
			if ($flag['widgets'] == false) {
				$message .= __('Error on Widgets import.','prolist');
			}
			$result['type'] = "error";
			$result['message'] = $message;
		}
	// Result
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      	$result = json_encode($result);
      	echo $result;
      	die;
   	}
   	else {
    	header("Location: ".$_SERVER["HTTP_REFERER"]);
   	}

	wp_die();
}
