<?php

/**
 * Class Prolist_Importer
 *
 * Base on Radium Importer. This class provides the capability to import demo content as well as import widgets and WordPress menus
 *
 * @since 1.0.0
 *
 * @package  prolist
 *
 */

 // Exit if accessed directly
 if ( !defined( 'ABSPATH' ) ) exit;

 // Don't duplicate me!
 if ( !class_exists( 'Prolist_Importer' ) ) {

	class Prolist_Importer {
		public $flag_as_imported = array(
			'content' => false,
			'menus' => false,
			'options' => false,
			'widgets' =>false
		);
		/**
		 * Holds a copy of the object for easy reference.
		 *
		 * @since 0.0.2
		 *
		 * @var object
		 */
		public $theme_options_file;

		public $wp_options_file;

		public $customizer_file;

		public $ess_file;

		public $rev_file;

		/**
		 * Holds a copy of the object for easy reference.
		 *
		 * @since 0.0.2
		 *
		 * @var object
		 */
		public $widgets;

		/**
		 * Holds a copy of the object for easy reference.
		 *
		 * @since 0.0.2
		 *
		 * @var object
		 */
		public $content_demo;
	    /**
	     * Holds a copy of the object for easy reference.
	     *
	     * @since 0.0.2
	     *
	     * @var object
	     */
	    private static $instance;

	    /**
	     * Constructor. Hooks all interactions to initialize the class.
	     *
	     * @since 0.0.2
	     */
	    public function __construct() {

	        self::$instance = $this;

	        $this->demo_files_path 		= apply_filters('prolist_theme_importer_demo_files_path', $this->demo_files_path);

	        //$this->theme_options_file 	= apply_filters('radium_theme_importer_theme_options_file', $this->demo_files_path . $this->theme_options_file_name);

	        $this->wp_options_file 		= apply_filters('prolist_theme_importer_wp_options_file', $this->demo_files_path . $this->wp_options_file_name);

	        $this->customizer_file 		= apply_filters('prolist_theme_importer_customizer_file', $this->demo_files_path . $this->customizer_file_name);

	        $this->widgets 				= apply_filters('prolist_theme_importer_widgets_file', $this->demo_files_path . $this->widgets_file_name);

	        $this->content_demo 		= apply_filters('prolist_theme_importer_content_demo_file', $this->demo_files_path . $this->content_demo_file_name);

	        $this->ess_file 		= apply_filters('prolist_theme_importer_ess_file', $this->demo_files_path . $this->ess_file_name);

	        $this->rev_file 		= apply_filters('prolist_theme_importer_rev_file', $this->rev_file_name);

	        //add_action( 'prolist_import_end', array( $this, 'after_wp_importer' ) );
	    }

	    /**
	     * Process all imports
	     *
	     * @params $content
	     * @params $menus
	     * @params $options
	     * @params $widgets
	     *
	     * @since 0.0.3
	     *
	     * @return null
	     */
	    public function process_imports() {

	    	if ( !empty( $this->content_demo ) && is_file( $this->content_demo ) ) {
				$this->set_demo_data( $this->content_demo );
			}
			/*
			if ( !empty( $this->theme_options_file ) && is_file( $this->theme_options_file ) ) {
				$this->set_demo_theme_options( $this->theme_options_file );
			}
			*/
			$this->set_wordpress_options( $this->wp_options_file );

			$this->set_customizer( $this->customizer_file );

			$this->set_demo_menus();

			if ( !empty( $this->widgets ) && is_file( $this->widgets ) ) {
				$this->process_widget_import_file( $this->widgets );
			}

			//$this->set_essential_grid( $this->ess_file ); // Un comment if you want use ESS Grid

			$this->set_rev_slider( $this->rev_file );

			do_action( 'prolist_import_end');

        }

	    /**
	     * add_widget_to_sidebar Import sidebars
	     * @param  string $sidebar_slug    Sidebar slug to add widget
	     * @param  string $widget_slug     Widget slug
	     * @param  string $count_mod       position in sidebar
	     * @param  array  $widget_settings widget settings
	     *
	     * @since 0.0.2
	     *
	     * @return null
	     */
	    public function add_widget_to_sidebar($sidebar_slug, $widget_slug, $count_mod, $widget_settings = array()){

	        $sidebars_widgets = get_option('sidebars_widgets');

	        if(!isset($sidebars_widgets[$sidebar_slug]))
	           $sidebars_widgets[$sidebar_slug] = array('_multiwidget' => 1);

	        $newWidget = get_option('widget_'.$widget_slug);

	        if(!is_array($newWidget))
	            $newWidget = array();

	        $count = count($newWidget)+1+$count_mod;
	        $sidebars_widgets[$sidebar_slug][] = $widget_slug.'-'.$count;

	        $newWidget[$count] = $widget_settings;

	        update_option('sidebars_widgets', $sidebars_widgets);
	        update_option('widget_'.$widget_slug, $newWidget);

	    }

	    public function set_demo_data( $file ) {

		    if ( !defined('WP_LOAD_IMPORTERS') ) define('WP_LOAD_IMPORTERS', true);

	        require_once ABSPATH . 'wp-admin/includes/import.php';

	        $importer_error = false;

	        if ( !class_exists( 'WP_Importer' ) ) {

	            $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';

	            if ( file_exists( $class_wp_importer ) ){

	                require_once($class_wp_importer);

	            } else {

	                $importer_error = true;

	            }

	        }

	        if ( !class_exists( 'WP_Import' ) ) {

	            $class_wp_import = dirname( __FILE__ ) .'/wordpress-importer.php';

	            if ( file_exists( $class_wp_import ) )
	                require_once($class_wp_import);
	            else
	                $importer_error = true;

	        }

	        if($importer_error){

	            die("Error on import");

	        } else {

	            if(!is_file( $file )){

	                echo "The XML file containing the dummy content is not available or could not be read .. You might want to try to set the file permission to chmod 755.<br/>If this doesn't work please use the Wordpress importer and import the XML file (should be located in your download .zip: Sample Content folder) manually ";

	            } else {

	               	$wp_import = new WP_Import();
	               	$wp_import->fetch_attachments = true;
	               	$wp_import->import( $file );
					$this->flag_as_imported['content'] = true;

	         	}

	    	}

	    	do_action( 'prolist_importer_after_theme_content_import');


	    }

	    public function set_demo_menus() {}

	    /**
		 * Get an array of known options which we would want checked by default when importing.
		 *
		 * @return array
		 */
		private function get_whitelist_options() {
			return apply_filters( 'options_import_whitelist', array(
				// 'active_plugins',
				'admin_email',
				'advanced_edit',
				'avatar_default',
				'avatar_rating',
				'blacklist_keys',
				'blogdescription',
				'blogname',
				'blog_charset',
				'blog_public',
				'blog_upload_space',
				'category_base',
				'category_children',
				'close_comments_days_old',
				'close_comments_for_old_posts',
				'comments_notify',
				'comments_per_page',
				'comment_max_links',
				'comment_moderation',
				'comment_order',
				'comment_registration',
				'comment_whitelist',
				'cron',
				// 'current_theme',
				'date_format',
				'default_category',
				'default_comments_page',
				'default_comment_status',
				'default_email_category',
				'default_link_category',
				'default_pingback_flag',
				'default_ping_status',
				'default_post_format',
				'default_role',
				'gmt_offset',
				'gzipcompression',
				'hack_file',
				'html_type',
				'image_default_align',
				'image_default_link_type',
				'image_default_size',
				'large_size_h',
				'large_size_w',
				'links_recently_updated_append',
				'links_recently_updated_prepend',
				'links_recently_updated_time',
				'links_updated_date_format',
				'link_manager_enabled',
				'mailserver_login',
				'mailserver_pass',
				'mailserver_port',
				'mailserver_url',
				'medium_size_h',
				'medium_size_w',
				'moderation_keys',
				'moderation_notify',
				'ms_robotstxt',
				'ms_robotstxt_sitemap',
				'nav_menu_options',
				'page_comments',
				'page_for_posts',
				'page_on_front',
				'permalink_structure',
				'ping_sites',
				'posts_per_page',
				'posts_per_rss',
				'recently_activated',
				'recently_edited',
				'require_name_email',
				'rss_use_excerpt',
				'show_avatars',
				'show_on_front',
				'sidebars_widgets',
				'start_of_week',
				'sticky_posts',
				// 'stylesheet',
				'subscription_options',
				'tag_base',
				// 'template',
				'theme_switched',
				'thread_comments',
				'thread_comments_depth',
				'thumbnail_crop',
				'thumbnail_size_h',
				'thumbnail_size_w',
				'timezone_string',
				'time_format',
				'uninstall_plugins',
				'uploads_use_yearmonth_folders',
				'upload_path',
				'upload_url_path',
				'users_can_register',
				'use_balanceTags',
				'use_smilies',
				'use_trackback',
				'widget_archives',
				'widget_categories',
				'widget_image',
				'widget_meta',
				'widget_nav_menu',
				'widget_recent-comments',
				'widget_recent-posts',
				'widget_rss',
				'widget_rss_links',
				'widget_search',
				'widget_text',
				'widget_top-posts',
				'WPLANG',
				'_taxonomy_font_icons',
				"job_manager_enable_categories",
		        "job_manager_enable_default_category_multiselect",
		        "job_manager_category_filter_type",
		        "job_manager_user_requires_account",
		        "job_manager_enable_registration",
		        "job_manager_generate_username_from_email",
		        "job_manager_registration_role",
		        "job_manager_submission_requires_approval",
		        "job_manager_user_can_edit_pending_submissions",
		        "job_manager_enable_tag_archive",
		        "job_manager_tags_filter_type",
		        "job_manager_max_tags",
		        "job_manager_tag_input",
		        "job_manager_regions_filter",
			) );
		}

	    public function set_wordpress_options( $file ){
	    	if ( file_exists( $file ) ) {
		    	$hash = '1adea3d3bf202a7bab8a388cfce650e6722fa18e';
		    	$options_to_import = array();

				$file_contents = file_get_contents($file);
				$import_data = json_decode($file_contents,true);
				/**
				$options_to_import = $this->get_whitelist_options();
				foreach ( (array) $options_to_import as $option_name ) {
					if ( !isset( $import_data['options'][ $option_name ] ) ) {
						// we're going to use a random hash as our default, to know if something is set or not
						$old_value = get_option( $option_name, $hash );

						// only import the setting if it's not present
						if ( $old_value !== $hash ) {
							//echo "\n<p>" . sprintf( __( 'Skipped option `%s` because it currently exists.', 'wp-options-importer' ), esc_html( $option_name ) ) . '</p>';
							continue;
						}

						$option_value = maybe_unserialize( $import_data['options'][ $option_name ] );
						if ( in_array( $option_name, $import_data['no_autoload'] ) ) {
							delete_option( $option_name );
							add_option( $option_name, $option_value, '', 'no' );
						} else {
							update_option( $option_name, $option_value );
						}
					}
				}
				*/
				$options = $import_data['options'];

				foreach ($options as $option_name => $value) {

					$option_value = maybe_unserialize( $value );

					update_option( $option_name, $option_value );
				}
			}
	    }

	    public function set_demo_theme_options( $file ) {

	    	// Does the File exist?
			if ( file_exists( $file ) ) {

				// Get file contents and decode
				$data = file_get_contents( $file );

				$data = maybe_unserialize( $data );

				// Only if there is data
				if ( !empty( $data ) || is_array( $data ) ) {

					// Hook before import
					$data = apply_filters( 'prolist_theme_import_theme_options', $data );

					update_option( $this->theme_option_name, $data );

					$this->flag_as_imported['options'] = true;
				}

	      		do_action( 'prolist_importer_after_theme_options_import', $this->active_import, $this->demo_files_path );

      		} else {

	      		wp_die(
      				__( 'Theme options Import file could not be found. Please try again.', 'prolist' ),
      				'',
      				array( 'back_link' => true )
      			);
       		}

	    }
	    /**
	     * Customizer
	     */
	    public function set_customizer( $file ) {
	    	// Does the File exist?
			if ( file_exists( $file ) ) {

				$template	= get_template();

		    	// Get the upload data.
				$raw  = file_get_contents( $file );
				$data = maybe_unserialize( $raw );

				// Data checks.
				if ( 'array' != gettype( $data ) ) {
					//$cei_error = __( 'Error importing settings! Please check that you uploaded a customizer export file.', 'customizer-export-import' );
					return;
				}
				if ( ! isset( $data['template'] ) || ! isset( $data['mods'] ) ) {
					//$cei_error = __( 'Error importing settings! Please check that you uploaded a customizer export file.', 'customizer-export-import' );
					return;
				}
				if ( $data['template'] != $template ) {
					//$cei_error = __( 'Error importing settings! The settings you uploaded are not for the current theme.', 'customizer-export-import' );
					return;
				}

				// Import images.
				$data['mods'] = self::_import_images( $data['mods'] );

				// Call the customize_save action.
				//do_action( 'customize_save', $wp_customize );

				// Loop through the mods.
				foreach ( $data['mods'] as $key => $val ) {

					// Call the customize_save_ dynamic action.
					//do_action( 'customize_save_' . $key, $wp_customize );

					// Save the mod.
					set_theme_mod( $key, $val );
				}

				// Call the customize_save_after action.
				//do_action( 'customize_save_after', $wp_customize );
			}
	    }
	    /**
		 * Imports images for settings saved as mods.
		 *
		 * @since 0.1
		 * @access private
		 * @param array $mods An array of customizer mods.
		 * @return array The mods array with any new import data.
		 */
		static private function _import_images( $mods )
		{
			foreach ( $mods as $key => $val ) {

				if ( self::_is_image_url( $val ) ) {

					$data = self::_sideload_image( $val );

					if ( ! is_wp_error( $data ) ) {

						$mods[ $key ] = $data->url;

						// Handle header image controls.
						if ( isset( $mods[ $key . '_data' ] ) ) {
							$mods[ $key . '_data' ] = $data;
							update_post_meta( $data->attachment_id, '_wp_attachment_is_custom_header', get_stylesheet() );
						}
					}
				}
			}

			return $mods;
		}

		/**
		 * Taken from the core media_sideload_image function and
		 * modified to return an array of data instead of html.
		 *
		 * @since 0.1
		 * @access private
		 * @param string $file The image file path.
		 * @return array An array of image data.
		 */
		static private function _sideload_image( $file )
		{
			$data = new stdClass();

			if ( ! function_exists( 'media_handle_sideload' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
			}
			if ( ! empty( $file ) ) {

				// Set variables for storage, fix file filename for query strings.
				preg_match( '/[^\?]+\.(jpe?g|jpe|gif|png)\b/i', $file, $matches );
				$file_array = array();
				$file_array['name'] = basename( $matches[0] );

				// Download file to temp location.
				$file_array['tmp_name'] = download_url( $file );

				// If error storing temporarily, return the error.
				if ( is_wp_error( $file_array['tmp_name'] ) ) {
					return $file_array['tmp_name'];
				}

				// Do the validation and storage stuff.
				$id = media_handle_sideload( $file_array, 0 );

				// If error storing permanently, unlink.
				if ( is_wp_error( $id ) ) {
					@unlink( $file_array['tmp_name'] );
					return $id;
				}

				// Build the object to return.
				$meta					= wp_get_attachment_metadata( $id );
				$data->attachment_id	= $id;
				$data->url				= wp_get_attachment_url( $id );
				$data->thumbnail_url	= wp_get_attachment_thumb_url( $id );
				$data->height			= $meta['height'];
				$data->width			= $meta['width'];
			}

			return $data;
		}

		/**
		 * Checks to see whether a string is an image url or not.
		 *
		 * @since 0.1
		 * @access private
		 * @param string $string The string to check.
		 * @return bool Whether the string is an image url or not.
		 */
		static private function _is_image_url( $string = '' )
		{
			if ( is_string( $string ) ) {

				if ( preg_match( '/\.(jpg|jpeg|png|gif)/i', $string ) ) {
					return true;
				}
			}

			return false;
		}
		/**
		 * Ess Grid
		 */
		public function set_essential_grid( $file ) {
			if ( file_exists( $file ) ) {
				if ( file_exists( ABSPATH . '/wp-content/plugins/essential-grid/essential-grid.php' ) ) {
					require_once(ABSPATH .'wp-content/plugins/essential-grid/essential-grid.php');
				}

				$raw  = file_get_contents( $file );
				$es_data = json_decode( $raw, true );

				try{
					$im = new Essential_Grid_Import();

					$overwriteData = array(
						'global-styles-overwrite' => 'overwrite'
					);

					// Create Overwrite & Ids data
					$skins = @$es_data['skins'];
					$export_skins = array();
					if(!empty($skins) && is_array($skins)){
						foreach ($skins as $skin) {
							$export_skins[] = $skin['id'];
							$overwriteData['skin-overwrite-' . $skin['id']] = 'overwrite';
						}
					}

					$export_navigation_skins = array();
					$navigation_skins = @$es_data['navigation-skins'];

					foreach ((array)$navigation_skins as $nav_skin) {
						$export_navigation_skins[] = $nav_skin['id'];
						$overwriteData['nav-skin-overwrite-' . $nav_skin['id']] = 'overwrite';
					}

					$export_grids = array();
					$grids = @$es_data['grids'];
					if(!empty($grids) && is_array($grids)){
						foreach ($grids as $grid) {
							$export_grids[] = $grid['id'];
							$overwriteData['grid-overwrite-' . $grid['id']] = 'overwrite';
						}
					}

					$export_elements = array();
					$elements = @$es_data['elements'];
					if (!empty($elements) && is_array($elements))
					{foreach ($elements as $element) {
						$export_elements[] = $element['id'];
						$overwriteData['elements-overwrite-' . $element['id']] = 'overwrite';
					}}

					$export_custom_meta = array();
					$custom_metas = @$es_data['custom-meta'];
					if(!empty($custom_metas) && is_array($custom_metas)){
						foreach ($custom_metas as $custom_meta) {
							$export_custom_meta[] = $custom_meta['handle'];
							$overwriteData['custom-meta-overwrite-' .  $custom_meta['handle']] = 'overwrite';
						}
					}

					$export_punch_fonts = array();
					$custom_fonts = @$es_data['punch-fonts'];
					if(!empty($custom_fonts) && is_array($custom_fonts)){
						foreach ($custom_fonts as $custom_font) {
							$export_punch_fonts[] = $custom_font['handle'];
							$overwriteData['punch-fonts-overwrite-' . $custom_font['handle']] = 'overwrite';
						}
					}

					$im->set_overwrite_data($overwriteData); //set overwrite data global to class

					// Import data
					$skins = @$es_data['skins'];
					if(!empty($skins) && is_array($skins)){
						if(!empty($skins)){
							$skins_imported = $im->import_skins($skins, $export_skins);
						}
					}

					$navigation_skins = @$es_data['navigation-skins'];
					if(!empty($navigation_skins) && is_array($navigation_skins)){
						if(!empty($navigation_skins)){
							$navigation_skins_imported = $im->import_navigation_skins(@$navigation_skins, $export_navigation_skins);
						}
					}

					$grids = @$es_data['grids'];
					if(!empty($grids) && is_array($grids)){
						if(!empty($grids)){
							$grids_imported = $im->import_grids($grids, $export_grids);
						}
					}

					$elements = @$es_data['elements'];
					if(!empty($elements) && is_array($elements)){
						if(!empty($elements)){
							$elements_imported = $im->import_elements(@$elements, $export_elements);
						}
					}

					$custom_metas = @$es_data['custom-meta'];
					if(!empty($custom_metas) && is_array($custom_metas)){
						if(!empty($custom_metas)){
							$custom_metas_imported = $im->import_custom_meta($custom_metas, $export_custom_meta);
						}
					}

					$custom_fonts = @$es_data['punch-fonts'];
					if(!empty($custom_fonts) && is_array($custom_fonts)){
						if(!empty($custom_fonts)){
							$custom_fonts_imported = $im->import_punch_fonts($custom_fonts, $export_punch_fonts);
						}
					}

					if(true){
						$global_css = @$es_data['global-css'];

						$tglobal_css = stripslashes($global_css);
						if(empty($tglobal_css)) {$tglobal_css = $global_css;}

						$global_styles_imported = $im->import_global_styles($tglobal_css);
					}
				}catch(Exception $d){
				}
			}
		}
		/**
		 * Rev Slider
		 */
		public function set_rev_slider( $file ) {
			if ( ! class_exists( 'RevSlider' ) ) {

				if ( file_exists( ABSPATH . '/wp-content/plugins/revslider/revslider.php' ) ) {

					require( ABSPATH .'wp-content/plugins/revslider/revslider.php' );

				}

			}
			if ( class_exists( 'RevSlider' ) ) {

					$rev_files = glob($this->demo_files_path . $file . '/*.zip');

					if (!empty($rev_files)) {
						foreach ($rev_files as $rev_file) {
							$_FILES['import_file']['error'] = UPLOAD_ERR_OK;
							$_FILES['import_file']['tmp_name']= $rev_file;

							$slider = new RevSlider();
							$slider->importSliderFromPost( true, 'none' );
						}
					}

			}
		}
	    /**
	     * Available widgets
	     *
	     * Gather site's widgets into array with ID base, name, etc.
	     * Used by export and import functions.
	     *
	     * @since 0.0.2
	     *
	     * @global array $wp_registered_widget_updates
	     * @return array Widget information
	     */
	    function available_widgets() {

	    	global $wp_registered_widget_controls;

	    	$widget_controls = $wp_registered_widget_controls;

	    	$available_widgets = array();

	    	foreach ( $widget_controls as $widget ) {

	    		if ( ! empty( $widget['id_base'] ) && ! isset( $available_widgets[$widget['id_base']] ) ) { // no dupes

	    			$available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
	    			$available_widgets[$widget['id_base']]['name'] = $widget['name'];

	    		}

	    	}

	    	return apply_filters( 'prolist_theme_import_widget_available_widgets', $available_widgets );

	    }


	    /**
	     * Process import file
	     *
	     * This parses a file and triggers importation of its widgets.
	     *
	     * @since 0.0.2
	     *
	     * @param string $file Path to .wie file uploaded
	     * @global string $widget_import_results
	     */
	    function process_widget_import_file( $file ) {

	    	// File exists?
	    	if ( ! file_exists( $file ) ) {
	    		wp_die(
	    			__( 'Widget Import file could not be found. Please try again.', 'prolist' ),
	    			'',
	    			array( 'back_link' => true )
	    		);
	    	}

	    	// Get file contents and decode
	    	$data = file_get_contents( $file );
	    	$data = json_decode( $data );

	    	// Delete import file
	    	//unlink( $file );

	    	// Import the widget data
	    	// Make results available for display on import/export page
	    	$this->widget_import_results = $this->import_widgets( $data );

	    }


	    /**
	     * Import widget JSON data
	     *
	     * @since 0.0.2
	     * @global array $wp_registered_sidebars
	     * @param object $data JSON widget data from .json file
	     * @return array Results array
	     */
	    public function import_widgets( $data ) {

	    	global $wp_registered_sidebars;

	    	// Have valid data?
	    	// If no data or could not decode
	    	if ( empty( $data ) || ! is_object( $data ) ) {
	    		return;
	    	}

	    	// Hook before import
	    	$data = apply_filters( 'prolist_theme_import_widget_data', $data );

	    	// Get all available widgets site supports
	    	$available_widgets = $this->available_widgets();

	    	// Get all existing widget instances
	    	$widget_instances = array();
	    	foreach ( $available_widgets as $widget_data ) {
	    		$widget_instances[$widget_data['id_base']] = get_option( 'widget_' . $widget_data['id_base'] );
	    	}

	    	// Begin results
	    	$results = array();

	    	// Loop import data's sidebars
	    	foreach ( $data as $sidebar_id => $widgets ) {

	    		// Skip inactive widgets
	    		// (should not be in export file)
	    		if ( 'wp_inactive_widgets' == $sidebar_id ) {
	    			continue;
	    		}

	    		// Check if sidebar is available on this site
	    		// Otherwise add widgets to inactive, and say so
	    		if ( isset( $wp_registered_sidebars[$sidebar_id] ) ) {
	    			$sidebar_available = true;
	    			$use_sidebar_id = $sidebar_id;
	    			$sidebar_message_type = 'success';
	    			$sidebar_message = '';
	    		} else {
	    			$sidebar_available = false;
	    			$use_sidebar_id = 'wp_inactive_widgets'; // add to inactive if sidebar does not exist in theme
	    			$sidebar_message_type = 'error';
	    			$sidebar_message = __( 'Sidebar does not exist in theme (using Inactive)', 'prolist' );
	    		}

	    		// Result for sidebar
	    		$results[$sidebar_id]['name'] = ! empty( $wp_registered_sidebars[$sidebar_id]['name'] ) ? $wp_registered_sidebars[$sidebar_id]['name'] : $sidebar_id; // sidebar name if theme supports it; otherwise ID
	    		$results[$sidebar_id]['message_type'] = $sidebar_message_type;
	    		$results[$sidebar_id]['message'] = $sidebar_message;
	    		$results[$sidebar_id]['widgets'] = array();

	    		// Loop widgets
	    		foreach ( $widgets as $widget_instance_id => $widget ) {

	    			$fail = false;

	    			// Get id_base (remove -# from end) and instance ID number
	    			$id_base = preg_replace( '/-[0-9]+$/', '', $widget_instance_id );
	    			$instance_id_number = str_replace( $id_base . '-', '', $widget_instance_id );

	    			// Does site support this widget?
	    			if ( ! $fail && ! isset( $available_widgets[$id_base] ) ) {
	    				$fail = true;
	    				$widget_message_type = 'error';
	    				$widget_message = __( 'Site does not support widget', 'prolist' ); // explain why widget not imported
	    			}

	    			// Filter to modify settings before import
	    			// Do before identical check because changes may make it identical to end result (such as URL replacements)
	    			$widget = apply_filters( 'prolist_theme_import_widget_settings', $widget );

	    			// Does widget with identical settings already exist in same sidebar?
	    			if ( ! $fail && isset( $widget_instances[$id_base] ) ) {

	    				// Get existing widgets in this sidebar
	    				$sidebars_widgets = get_option( 'sidebars_widgets' );
	    				$sidebar_widgets = isset( $sidebars_widgets[$use_sidebar_id] ) ? $sidebars_widgets[$use_sidebar_id] : array(); // check Inactive if that's where will go

	    				// Loop widgets with ID base
	    				$single_widget_instances = ! empty( $widget_instances[$id_base] ) ? $widget_instances[$id_base] : array();
	    				foreach ( $single_widget_instances as $check_id => $check_widget ) {

	    					// Is widget in same sidebar and has identical settings?
	    					if ( in_array( "$id_base-$check_id", $sidebar_widgets ) && (array) $widget == $check_widget ) {

	    						$fail = true;
	    						$widget_message_type = 'warning';
	    						$widget_message = __( 'Widget already exists', 'prolist' ); // explain why widget not imported

	    						break;

	    					}

	    				}

	    			}

	    			// No failure
	    			if ( ! $fail ) {

	    				// Add widget instance
	    				$single_widget_instances = get_option( 'widget_' . $id_base ); // all instances for that widget ID base, get fresh every time
	    				$single_widget_instances = ! empty( $single_widget_instances ) ? $single_widget_instances : array( '_multiwidget' => 1 ); // start fresh if have to
	    				$single_widget_instances[] = (array) $widget; // add it

    					// Get the key it was given
    					end( $single_widget_instances );
    					$new_instance_id_number = key( $single_widget_instances );

    					// If key is 0, make it 1
    					// When 0, an issue can occur where adding a widget causes data from other widget to load, and the widget doesn't stick (reload wipes it)
    					if ( '0' === strval( $new_instance_id_number ) ) {
    						$new_instance_id_number = 1;
    						$single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
    						unset( $single_widget_instances[0] );
    					}

    					// Move _multiwidget to end of array for uniformity
    					if ( isset( $single_widget_instances['_multiwidget'] ) ) {
    						$multiwidget = $single_widget_instances['_multiwidget'];
    						unset( $single_widget_instances['_multiwidget'] );
    						$single_widget_instances['_multiwidget'] = $multiwidget;
    					}

    					// Update option with new widget
    					update_option( 'widget_' . $id_base, $single_widget_instances );

	    				// Assign widget instance to sidebar
	    				$sidebars_widgets = get_option( 'sidebars_widgets' ); // which sidebars have which widgets, get fresh every time
	    				$new_instance_id = $id_base . '-' . $new_instance_id_number; // use ID number from new widget instance
	    				$sidebars_widgets[$use_sidebar_id][] = $new_instance_id; // add new instance to sidebar
	    				update_option( 'sidebars_widgets', $sidebars_widgets ); // save the amended data

	    				// Success message
	    				if ( $sidebar_available ) {
	    					$widget_message_type = 'success';
	    					$widget_message = __( 'Imported', 'prolist' );
	    				} else {
	    					$widget_message_type = 'warning';
	    					$widget_message = __( 'Imported to Inactive', 'prolist' );
	    				}

	    			}

	    			// Result for widget instance
	    			$results[$sidebar_id]['widgets'][$widget_instance_id]['name'] = isset( $available_widgets[$id_base]['name'] ) ? $available_widgets[$id_base]['name'] : $id_base; // widget name or ID if name not available (not supported by site)
	    			$results[$sidebar_id]['widgets'][$widget_instance_id]['title'] = $widget->title ? $widget->title : __( 'No Title', 'prolist' ); // show "No Title" if widget instance is untitled
	    			$results[$sidebar_id]['widgets'][$widget_instance_id]['message_type'] = $widget_message_type;
	    			$results[$sidebar_id]['widgets'][$widget_instance_id]['message'] = $widget_message;

	    		}

	    	}

			$this->flag_as_imported['widgets'] = true;

	    	// Hook after import
	    	do_action( 'prolist_theme_import_widget_after_import' );

	    	// Return results
	    	return apply_filters( 'prolist_theme_import_widget_results', $results );

	    }

	}//class

}//function_exists
