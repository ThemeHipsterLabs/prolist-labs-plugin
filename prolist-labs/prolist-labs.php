<?php
/**
 * @package ProList
 */
/*
Plugin Name: Prolist Labs
Plugin URI: http://prolistwp.com/
Description: Rocket for stunning theme.
Version: 1.0.1
Author: ProListWp
Author URI: http://prolistwp.com/
License: GPLv2 or later
Text Domain: prolist
*/

/**
 * Importer
 */
if ( file_exists( dirname( __FILE__ ) . '/install/install-init.php' ) ) {
    require_once dirname( __FILE__ ) . '/install/install-init.php';
}

/**
 * Widget
 */

$files = glob(dirname( __FILE__ ) . '/widget/*.php');

if (!empty($files)) {
	foreach ($files as $file) {
		require_once $file;
	}
}

/**
 * Wp Job Manager Taxonomy label
 */
add_filter( 'register_taxonomy_job_listing_category_args', 'prolist_change_taxonomy_job_listing_category_args' );

function prolist_change_taxonomy_job_listing_category_args( $args ) {
	$singular = esc_html__( 'Listing Category', 'prolist' );
	$plural   = esc_html__( 'Listing Categories', 'prolist' );

	$args['label'] = $plural;

	$args['labels'] = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'menu_name'         => esc_html__( 'Categories', 'prolist' ),
		'search_items'      => sprintf( esc_html__( 'Search %s', 'prolist' ), $plural ),
		'all_items'         => sprintf( esc_html__( 'All %s', 'prolist' ), $plural ),
		'parent_item'       => sprintf( esc_html__( 'Parent %s', 'prolist' ), $singular ),
		'parent_item_colon' => sprintf( esc_html__( 'Parent %s:', 'prolist' ), $singular ),
		'edit_item'         => sprintf( esc_html__( 'Edit %s', 'prolist' ), $singular ),
		'update_item'       => sprintf( esc_html__( 'Update %s', 'prolist' ), $singular ),
		'add_new_item'      => sprintf( esc_html__( 'Add New %s', 'prolist' ), $singular ),
		'new_item_name'     => sprintf( esc_html__( 'New %s Name', 'prolist' ), $singular )
	);

	if ( isset( $args['rewrite'] ) && is_array( $args['rewrite'] ) ) {
		$args['rewrite']['slug'] = _x( 'listing-category', 'Listing category Slug. Go to Save permalink after changing this', 'prolist' );
	}

	$permalinks = get_option( 'prolist_permalinks_settings' );

	if ( isset( $permalinks['category_base'] ) && ! empty( $permalinks['category_base'] ) ) {
		$args['rewrite']['slug'] = $permalinks['category_base'];
	}

	return $args;
}

add_filter( 'register_taxonomy_job_listing_type_args', 'prolist_change_taxonomy_job_listing_type_args' );

function prolist_change_taxonomy_job_listing_type_args( $args ) {
	$singular = esc_html__( 'Listing Type', 'prolist' );
	$plural   = esc_html__( 'Listing Types', 'prolist' );

	$args['label']  = $plural;
	$args['labels'] = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'menu_name'         => esc_html__( 'Types', 'prolist' ),
		'search_items'      => sprintf( esc_html__( 'Search %s', 'prolist' ), $plural ),
		'all_items'         => sprintf( esc_html__( 'All %s', 'prolist' ), $plural ),
		'parent_item'       => sprintf( esc_html__( 'Parent %s', 'prolist' ), $singular ),
		'parent_item_colon' => sprintf( esc_html__( 'Parent %s:', 'prolist' ), $singular ),
		'edit_item'         => sprintf( esc_html__( 'Edit %s', 'prolist' ), $singular ),
		'update_item'       => sprintf( esc_html__( 'Update %s', 'prolist' ), $singular ),
		'add_new_item'      => sprintf( esc_html__( 'Add New %s', 'prolist' ), $singular ),
		'new_item_name'     => sprintf( esc_html__( 'New %s Name', 'prolist' ), $singular )
	);

	if ( isset( $args['rewrite'] ) && is_array( $args['rewrite'] ) ) {
		$args['rewrite']['slug'] = _x( 'listing-type', 'Listing type Slug. Go to Save permalink after changing.', 'prolist' );
	}

	return $args;
}

add_action( 'init', 'prolist_replace_listing_tags_label' );

function prolist_replace_listing_tags_label() {

	global $wp_taxonomies;

	if ( ! isset( $wp_taxonomies['job_listing_tag'] ) ) {
		return;
	}
	$job_listing_tag_args = get_taxonomy( 'job_listing_tag' );

	$labels = &$job_listing_tag_args->labels;

	$labels->name                       = esc_html__( 'Listing Tags', 'prolist' );
	$labels->singular_name              = esc_html__( 'Listing Tag', 'prolist' );
	$labels->search_items               = esc_html__( 'Search Listing Tags', 'prolist' );
	$labels->popular_items              = esc_html__( 'Popular Tags', 'prolist' );
	$labels->all_items                  = esc_html__( 'All Listing Tags', 'prolist' );
	$labels->parent_item                = esc_html__( 'Parent Listing Tag', 'prolist' );
	$labels->parent_item_colon          = esc_html__( 'Parent Listing Tag:', 'prolist' );
	$labels->edit_item                  = esc_html__( 'Edit Listing Tag', 'prolist' );
	$labels->view_item                  = esc_html__( 'View Tag', 'prolist' );
	$labels->update_item                = esc_html__( 'Update Listing Tag', 'prolist' );
	$labels->add_new_item               = esc_html__( 'Add New Listing Tag', 'prolist' );
	$labels->new_item_name              = esc_html__( 'New Listing Tag Name', 'prolist' );
	$labels->separate_items_with_commas = esc_html__( 'Separate tags with commas', 'prolist' );
	$labels->add_or_remove_items        = esc_html__( 'Add or remove tags', 'prolist' );
	$labels->choose_from_most_used      = esc_html__( 'Choose from the most used tags', 'prolist' );
	$labels->not_found                  = esc_html__( 'No tags found.', 'prolist' );
	$labels->no_terms                   = esc_html__( 'No tags', 'prolist' );
	$labels->menu_name                  = esc_html__( 'Listing Tags', 'prolist' );
	$labels->name_admin_bar             = esc_html__( 'Listing Tag', 'prolist' );

	$job_listing_tag_args->rewrite = array(
		'slug'         => _x( 'listing-tag', 'permalink', 'prolist' ),
		'with_front'   => false,
		'ep_mask'      => 0,
		'hierarchical' => false
	);

	$permalinks = get_option( 'prolist_permalinks_settings' );
	if ( isset( $permalinks['tag_base'] ) && ! empty( $permalinks['tag_base'] ) ) {
		$job_listing_tag_args->rewrite['slug'] = $permalinks['tag_base'];
	}
	register_taxonomy( 'job_listing_tag', array( 'job_listing' ), (array) $job_listing_tag_args );
}

add_action( 'init', 'prolist_replace_listing_regions_label', 11 );

function prolist_replace_listing_regions_label() {

	global $wp_taxonomies;

	if ( ! isset( $wp_taxonomies['job_listing_region'] ) ) {
		return;
	}

	$job_listing_region_args = get_taxonomy( 'job_listing_region' );

	$labels = &$job_listing_region_args->labels;

	$labels->name                       = esc_html__( 'Listing Regions', 'prolist' );
	$labels->singular_name              = esc_html__( 'Region', 'prolist' );
	$labels->search_items               = esc_html__( 'Search Regions', 'prolist' );
	$labels->popular_items              = esc_html__( 'Popular Regions', 'prolist' );
	$labels->all_items                  = esc_html__( 'All Regions', 'prolist' );
	$labels->parent_item                = esc_html__( 'Parent Region', 'prolist' );
	$labels->parent_item_colon          = esc_html__( 'Parent Region:', 'prolist' );
	$labels->edit_item                  = esc_html__( 'Edit Region', 'prolist' );
	$labels->view_item                  = esc_html__( 'View Region', 'prolist' );
	$labels->update_item                = esc_html__( 'Update Region', 'prolist' );
	$labels->add_new_item               = esc_html__( 'Add New Region', 'prolist' );
	$labels->new_item_name              = esc_html__( 'New Region Name', 'prolist' );
	$labels->separate_items_with_commas = esc_html__( 'Separate regions with commas', 'prolist' );
	$labels->add_or_remove_items        = esc_html__( 'Add or remove regions', 'prolist' );
	$labels->choose_from_most_used      = esc_html__( 'Choose from the most used regions', 'prolist' );
	$labels->not_found                  = esc_html__( 'No regions found.', 'prolist' );
	$labels->no_terms                   = esc_html__( 'No regions', 'prolist' );
	$labels->menu_name                  = esc_html__( 'Regions', 'prolist' );
	$labels->name_admin_bar             = esc_html__( 'Listing Region', 'prolist' );
	$job_listing_region_args->label     = esc_html__( 'Listing Regions', 'prolist' );

	$job_listing_region_args->rewrite = array(
		'slug'         => _x( 'listing-region', 'permalink', 'prolist' ),
		'with_front'   => false,
		'ep_mask'      => 0,
		'hierarchical' => true
	);

	register_taxonomy( 'job_listing_region', array( 'job_listing' ), (array) $job_listing_region_args );
}
