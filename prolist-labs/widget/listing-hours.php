<?php
/**
 * ProList Listing Hours Widget
 *
 * @package ProList
 */
add_action( 'widgets_init', create_function( '', 'register_widget("ProList_Listing_Hours_Widget");' ) );
class ProList_Listing_Hours_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct( 'prolist_listing_hours', esc_html__( 'Prolist Listing Hours', 'prolist' ),array( 'description' => esc_html__( 'The listing open hours.', 'prolist' )));
	}

	function update($new_instance, $old_instance)
	{
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

	function form($instance)
	{
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = '';
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:','prolist' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
		echo '<p>' . $this->widget_options['description'] . '</p>';
	}

	function widget($args, $instance)
	{
		extract($args);
		$widget_title = apply_filters( 'widget_title', $instance['title'] );
		$widget_title = $args['before_title'] . $widget_title . $args['after_title'];
		echo $before_widget;
		$widget_id = "widget_" . $args["widget_id"];
		include dirname(__FILE__) . "/templates/listing-hours.php";
		echo $after_widget;
	}
}