<?php
/**
 * ProList Element Hero Image
 *
 * @package ProList
 */
add_action( 'widgets_init', create_function( '', 'register_widget("ProList_Element_Hero_Image_Widget");' ) );
class ProList_Element_Hero_Image_Widget extends WP_Widget
{
    /**
     * Constructor
     **/
    public function __construct()
    {
        $widget_ops = array(
            'classname' => 'prolist_element_hero_image',
            'description' => esc_html__( 'Widget that uses the built in Media library.', 'prolist' ),
        );

        parent::__construct( 'prolist_element_hero_image', esc_html__( 'Prolist Element Hero Image', 'prolist' ), $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
    }

    /**
     * Upload the Javascripts for the media uploader
     */
    public function upload_scripts()
    {
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . 'js/upload-media.js', array('jquery'));

        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . 'js/upload-media.js', array('jquery'));
        wp_enqueue_style('thickbox');
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    public function widget( $args, $instance )
    {
        extract($args);
        $widget_title = apply_filters( 'widget_title', $instance['title'] );
        $widget_title = '<h1>' . $widget_title . '</h1>';
        echo $before_widget;
        $widget_id = "widget_" . $args["widget_id"];
        include dirname(__FILE__) . "/templates/element-hero-image.php";
        echo $after_widget;
    }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    public function update( $new_instance, $old_instance ) {

        // update logic goes here
        $updated_instance = $new_instance;
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['image'] = ( ! empty( $new_instance['image'] ) ) ? strip_tags( $new_instance['image'] ) : '';
        return $updated_instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void
     **/
    public function form( $instance )
    {
        $title = '';
        if(isset($instance['title']))
        {
            $title = $instance['title'];
        }

        $image = '';
        if(isset($instance['image']))
        {
            $image = $instance['image'];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_name( 'title' ); ?>"><?php _e( 'Title:','prolist' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_name( 'image' ); ?>"><?php _e( 'Image:','prolist' ); ?></label>
            <p>
                <input name="<?php echo $this->get_field_name( 'image' ); ?>" id="<?php echo $this->get_field_id('image'); ?>" class="widefat custom_media_url" type="text" size="36"  value="<?php echo esc_url( $image ); ?>" />
                <img class="custom_media_url" src="<?php echo esc_url( $image ); ?>">
            </p>
            <input type="button" value="<?php _e( 'Upload Image', 'prolist' ); ?>" class="button custom_media_upload" id="custom_image_uploader"/>
        </p>
        <style type="text/css">
            .custom_media_url { max-width: 100%;height: auto;margin-top: 5px; }
        </style>
    <?php
    }
}
