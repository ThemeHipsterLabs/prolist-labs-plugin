<div class="clearfix">

	<address class="location" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<span class="icon_pin"></span>
		<?php echo esc_attr(get_post_meta( get_the_ID(), '_job_location', true )); ?>
		<meta itemprop="streetAddress" content="<?php echo trim( esc_attr(get_post_meta( get_the_ID(), 'geolocation_street_number', true )), '' ); ?> <?php echo trim( esc_attr(get_post_meta( get_the_ID(), 'geolocation_street', true ), '' )); ?>">
		<meta itemprop="addressRegion" content="<?php echo trim( esc_attr(get_post_meta( get_the_ID(), 'geolocation_state', true )), '' ); ?>">
		<meta itemprop="addressLocality" content="<?php echo trim( esc_attr(get_post_meta( get_the_ID(), 'geolocation_city', true )), '' ); ?>">
		<meta itemprop="postalCode" content="<?php echo trim( esc_attr(get_post_meta( get_the_ID(), 'geolocation_postcode', true )), '' ); ?>">
		<meta itemprop="addressCountry" content="<?php echo trim( esc_attr(get_post_meta( get_the_ID(), 'geolocation_country_short', true )), '' ); ?>">
	</address>

</div>