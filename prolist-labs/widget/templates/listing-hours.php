<?php echo $widget_title; ?>

<?php $business_hours = get_post_meta( get_the_ID(), '_job_hours', true ); ?>

<div class="open_hours" itemprop="openingHours"><?php echo esc_html( $business_hours ); ?></div>