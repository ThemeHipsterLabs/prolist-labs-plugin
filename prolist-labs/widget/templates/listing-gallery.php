<?php echo $widget_title; ?>
<div class="clearfix">
	<?php $gallery  = esc_html( get_post_meta( get_the_ID(), 'listing_gallery', true ) );?>
	<?php echo do_shortcode('[gallery columns="2" ids="'.$gallery.'"]'); ?>
</div>