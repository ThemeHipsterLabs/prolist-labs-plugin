<div class="text-right">
	<ul class="listing_doings clearfix">
		<?php if (class_exists('WP_Job_Manager_Claim_Listing')):?>
		<li>
			<?php if (class_exists('LoginWithAjax')):?>
				<a href="<?php echo prolist_get_login_url(); ?>" class="claim-listing open-popup-link"><?php _e( 'Claim this listing','prolist' );?></a>
			<?php else:?>
				<?php echo WP_Job_Manager_Claim_Listing()->listing->claim_listing_link(); ?>
			<?php endif;?>
		</li>
		<?php endif;?>

		<?php if (class_exists('WP_Job_Manager_Bookmarks')):?>
			<?php prolist_bookmark_form();?>
		<?php endif;?>
	</ul>
</div>
<?php
function prolist_bookmark_form(){
	global $job_manager_bookmarks;
	echo $job_manager_bookmarks->bookmark_form();
}
?>