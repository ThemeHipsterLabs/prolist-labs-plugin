<?php echo $widget_title; ?>
<?php
$terms = get_the_terms( get_the_ID(), 'job_listing_tag' );

if ( $terms && ! is_wp_error( $terms ) ) :?>

<ul class="listing-tags clearfix">

	<?php foreach ( $terms as $term ):?>

		<li><a title="<?php echo esc_html( $term->name ); ?>" href="<?php echo esc_url( get_term_link( $term, 'job_listing_tag' ) ); ?>"><i class="material-icons"><?php prolist_the_term_icon( $term->term_id,'job_listing_tag' ); ?></i> <?php echo esc_html( $term->name ); ?></a></li>

	<?php endforeach;?>

</ul>

<?php endif; ?>