<div class="text-right">

	<?php
		$twitter = esc_attr( get_post_meta( get_the_ID(), '_company_twitter', true) );
		$phone = esc_attr( get_post_meta( get_the_ID(), '_company_phone', true));
	?>

	<ul class="meta_info clearfix">
		<?php
		if ( ! empty( $phone ) ) :
			if ( strlen( $phone ) > 25 ) : ?>
				<li><i class="material-icons">phone_iphone</i> <a href="tel:<?php echo $phone; ?>" itemprop="telephone"><?php esc_html_e( 'Phone', 'prolist' ); ?></a></li>
			<?php else : ?>
				<li><i class="material-icons">phone_iphone</i> <a href="tel:<?php echo $phone; ?>" itemprop="telephone"><?php echo $phone; ?></a></li>
			<?php endif; ?>
		<?php endif;?>
		<?php
		if ( $website = get_the_company_website() ):
			$website_url = preg_replace('#^https?://#', '', rtrim(esc_url($website),'/'));
			if ( strlen( $website_url ) > 25 ) : ?>
				<li><i class="material-icons">link</i> <a href="<?php echo esc_url( $website ); ?>" itemprop="url" target="_blank" rel="nofollow"><?php esc_html_e( 'Our website', 'prolist' ); ?></a></li>
			<?php else : ?>
				<li><i class="material-icons">link</i> <a href="<?php echo esc_url( $website ); ?>" itemprop="url" target="_blank" rel="nofollow"><?php echo $website_url; ?></a></li>
			<?php endif; ?>
		<?php endif;?>
		<?php if ( ! empty( $twitter ) ):
			$twitter = preg_replace("[@]", "", $twitter);
			if ( strlen( $twitter ) > 30 ) : ?>
				<li><i class="social_twitter"></i> <a href="https://twitter.com/<?php echo $twitter; ?>" itemprop="url"> <?php esc_html_e( 'Twitter', 'prolist' ); ?></a></li>
			<?php else : ?>
				<li><i class="social_twitter"></i> <a href="https://twitter.com/<?php echo $twitter; ?>" itemprop="url"><?php echo $twitter; ?></a></li>
			<?php endif; ?>
		<?php endif; ?>

	</ul>

</div>