<div class="hero-image" style="background-image: url(<?php echo esc_url($instance['image']);?>)">
	<div class="container text-center">
		<div class="col-md-8 col-md-offset-2">
		<?php echo $widget_title; ?>
		</div>
	</div>
</div>