<?php
$products_ids = get_post_meta( get_the_ID(), '_products', true );

if ( ! empty( $products_ids ) ) :

	$query = apply_filters( 'woocommerce_related_products_args', array(
			'post_type'            	=> 'product',
			'post__in'             	=> $products_ids,
			'ignore_sticky_posts'  	=> 1,
			'no_found_rows'        	=> 1,
			'posts_per_page'       	=> -1,
	) );
	$bookable_products = new WP_Query( $query );

	echo $widget_title;

	if ( $bookable_products->have_posts() ) : ?>

		<div class="listing-booking-widget clearfix">
			<?php
			while ( $bookable_products->have_posts() ) : $bookable_products->the_post();

				woocommerce_template_single_add_to_cart();

			endwhile;
			wp_reset_postdata(); ?>
		</div><!-- .listing-products__items -->
		<?php

	endif;

endif;