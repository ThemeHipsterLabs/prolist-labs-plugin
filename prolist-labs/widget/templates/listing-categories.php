<?php
/**
 * ProList Listing Categories
 *
 * @package ProList
 */

$terms = get_the_terms( get_the_ID(), 'job_listing_category' );

if ( $terms && ! is_wp_error( $terms ) ) :?>

<ul class="listing-categories clearfix">

	<?php foreach ( $terms as $term ):?>

		<li><a title="<?php echo esc_html( $term->name ); ?>" href="<?php echo esc_url( get_term_link( $term,'job_listing_category' ) ); ?>"><i class="material-icons"><?php prolist_the_term_icon( $term->term_id, 'job_listing_category' ); ?></i></a></li>

	<?php endforeach;?>

</ul>

<?php endif;?>