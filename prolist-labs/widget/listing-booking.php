<?php
/**
 * ProList Listing Booking Widget
 *
 * @package ProList
 */
add_action( 'widgets_init', create_function( '', 'register_widget("ProList_Listing_Booking_Widget");' ) );
class ProList_Listing_Booking_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct( 'prolist_listing_booking', esc_html__( 'Prolist Listing Booking', 'prolist' ),array( 'description' => esc_html__( 'The listing booking.', 'prolist' )));
	}

	function update($new_instance, $old_instance)
	{
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

	function form($instance)
	{
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = '';
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:','prolist' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
		echo '<p>' . $this->widget_options['description'] . '</p>';
	}

	function widget($args, $instance)
	{
		extract($args);
		$widget_title = apply_filters( 'widget_title', $instance['title'] );
		$widget_title = $args['before_title'] . $widget_title . $args['after_title'];
		echo $before_widget;
		$widget_id = "widget_" . $args["widget_id"];
		include dirname(__FILE__) . "/templates/listing-booking.php";
		echo $after_widget;
	}
}
/**
 * Remove product listing in single listing
 */

add_action( 'single_job_listing_end', 'prolist_remove_products_listing', 1 );
function prolist_remove_products_listing() {
	if ( class_exists( 'WP_Job_Manager_Products' ) ) {
		$wp_job_manager_instance = WP_Job_Manager_Products::instance();
		remove_action( 'single_job_listing_end', array( $wp_job_manager_instance->products, 'listing_display_products' ) );
	}
}
