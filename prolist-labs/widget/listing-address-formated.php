<?php
/**
 * ProList Listing Rating Widget
 *
 * @package ProList
 */
add_action( 'widgets_init', create_function( '', 'register_widget("ProList_Listing_Address_Formated_Widget");' ) );
class ProList_Listing_Address_Formated_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct( 'prolist_listing_address_formated', esc_html__( 'Prolist Listing Address Formated', 'prolist' ),array( 'description' => esc_html__( 'The listing address formated.', 'prolist' )));
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		echo '<p>' . $this->widget_options['description'] . '</p>';
	}

	function widget($args, $instance)
	{
		extract($args);
		echo $before_widget;
		$widget_id = "widget_" . $args["widget_id"];
		include dirname(__FILE__) . "/templates/listing-address-formated.php";
		echo $after_widget;
	}
}