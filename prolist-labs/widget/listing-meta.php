<?php
/**
 * ProList Listing Meta Widget
 *
 * @package ProList
 */
add_action( 'widgets_init', create_function( '', 'register_widget("ProList_Listing_Meta_Widget");' ) );
class ProList_Listing_Meta_Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct( 'prolist_listing_meta', esc_html__( 'Prolist Listing Meta', 'prolist' ),array( 'description' => esc_html__( 'The listing meta.', 'prolist' )));
	}

	function update($new_instance, $old_instance)
	{
		return $new_instance;
	}

	function form($instance)
	{
		echo '<p>' . $this->widget_options['description'] . '</p>';
	}

	function widget($args, $instance)
	{
		extract($args);
		echo $before_widget;
		$widget_id = "widget_" . $args["widget_id"];
		include dirname(__FILE__) . "/templates/listing-meta.php";
		echo $after_widget;
	}
}