(function( $ ) {
	"use strict";
	$('.font-icon-picker').fontIconPicker({
	    theme             : 'fip-bootstrap',              // The CSS theme to use with this fontIconPicker. You can set different themes on multiple elements on the same page
	});
})( jQuery );