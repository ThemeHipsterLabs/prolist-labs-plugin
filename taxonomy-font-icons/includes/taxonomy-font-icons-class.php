<?php
/**
 * Plugin main class.
 *
 * @since 1.0
 */
class Taxonomy_Font_Icons {

	// Public variables
	public $taxonomy_font_icons_option;
	public $taxonomies;
	public $font;
	public $stylesheet;
	public $icons;

	function __construct() {

		// Set the wp_options option name
		$this->taxonomy_font_icons_option = '_taxonomy_font_icons';

		// Plugin init
		add_action( 'init', array( $this, 'plugin_init' ) );
	}

	/**
	 * Plugin activation.
	 *
	 * Creates the '_taxonomy_font_icons' wp_option if it doesn't exist.
	 *
	 * @since 1.0
	 */
	public function plugin_activation() {

		$tfi_icons = get_option( '_taxonomy_font_icons' );

		if ( empty( $tfi_icons ) ) {
			update_option( '_taxonomy_font_icons', '' );
		}
	}

	/**
	 * Plugin unistall.
	 *
	 * Remove the '_taxonomy_font_icons' option from wp_options.
	 *
	 * @since 1.0
	 */
	public function plugin_uninstall() {

		delete_option( '_taxonomy_font_icons' );
	}

	/**
	 * Plugin init.
	 *
	 * @since 1.0
	 */
	public function plugin_init() {

		// Get the default args
		$args = $this->default_args();

		// Set up the public variables
		$this->taxonomies = $args['taxonomies'];
		//$this->font       = $args['font'];
		//$this->stylesheet = $args['stylesheet'];
		//$this->icons      = $args['icons'];

		// Add icon select to the defined taxonomies
		foreach ( $this->taxonomies as $taxonomy ) {

			// Unfancy gets the column
			add_filter( "manage_edit-{$taxonomy}_columns",          array( $this, 'add_column_header' ) );
			add_filter( "manage_{$taxonomy}_custom_column",         array( $this, 'add_column_value'  ), 10, 3 );
			add_filter( "manage_edit-{$taxonomy}_sortable_columns", array( $this, 'sortable_columns'  ) );

			// Add icon select
			add_action( $taxonomy . '_add_form_fields', array( $this, 'add_taxonomy_icon' ) );
			add_action( $taxonomy . '_edit_form_fields', array( $this, 'edit_taxonomy_icon' ) );

			// Save icon to wp_options
			add_action( 'edited_' . $taxonomy, array( $this, 'save_taxonomy_icon' ), 10, 2 );
			add_action( 'create_' . $taxonomy, array( $this, 'save_taxonomy_icon' ), 10, 2 );
		}

		// Enqueue stylesheets
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_stylesheets' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_stylesheets' ) );
	}

	/**
	 * Add the "Color" column to taxonomy terms list-tables
	 *
	 * @since 0.1.0
	 *
	 * @param array $columns
	 *
	 * @return array
	 */
	public function add_column_header( $columns = array() ) {
		$columns['icon'] = __( 'Icon', 'taxonomy_icon_font' );

		return $columns;
	}

	/**
	 * Output the value for the custom column, in our case: `icon`
	 *
	 * @since 0.1.0
	 *
	 * @param string $empty
	 * @param string $custom_column
	 * @param int    $term_id
	 *
	 * @return mixed
	 */
	public function add_column_value( $empty = '', $custom_column = '', $term_id = 0 ) {

		// Bail if no taxonomy passed or not on the `icon` column
		if ( empty( $_REQUEST['taxonomy'] ) || ( 'icon' !== $custom_column ) || ! empty( $empty ) ) {
			return;
		}

		// Get the icon
		$icon  = $this->get_term_icon( $term_id, $_REQUEST['taxonomy'] );
		$retval = '&#8212;';

		// Output HTML element if not empty
		if ( ! empty( $icon ) ) {
			$retval = '<i data-icon="' . $icon . '" class="' . esc_attr( $icon ) . '"></i>';
		}

		echo $retval;
	}

	/**
	 * Allow sorting by `icon`
	 *
	 * @since 0.1.0
	 *
	 * @param array $columns
	 *
	 * @return array
	 */
	public function sortable_columns( $columns = array() ) {
		$columns['icon'] = 'icon';
		return $columns;
	}

	/**
	 * Return the icon of a term
	 *
	 * @since 0.1.0
	 *
	 * @param int $term_id
	 */
	public function get_term_icon( $term_id = 0, $taxonomy ) {
		$taxonomy = get_taxonomy( $taxonomy );
		$term = get_term( $term_id, $taxonomy->name );
		$term_slug = $term->slug;
		// Get the icons
		$term_meta = get_option( $this->taxonomy_font_icons_option );

		if ( ! empty( $term_meta ) ) {
			// Get the icon of the taxonomy
			$term_icon = ( isset( $term_meta[ $term_slug] ) ) ? $term_meta[$term_slug] : '';
		}else{
			$term_icon = '';
		}
		return $term_icon;
	}

	/**
	 * Default args.
	 *
	 * Use apply_filters( 'taxicon_filters_default_args', $args ); to modify the default values.
	 *
	 * @since 1.0
	 */
	public function default_args() {

		$args = array(
			'taxonomies' => array( 'job_listing_category', 'job_listing_tag' ),
			//'font'       => 'FontAwesome',
			//'stylesheet' => 'http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"',
			);

		$args = apply_filters( 'tfi_filters_default_args', $args );

		return $args;
	}

	/**
	 * Add new taxonomy.
	 *
	 * @since 1.0
	 */
	public function add_taxonomy_icon( $term ) {

		?>

		<div class="form-field">
			<label for="icons" style="padding-bottom: 10px;"><?php _e( 'Icon','taxonomy_icon_font' ); ?></label>

			<select data-bv-notempty="true" data-bv-notempty-message="<?php _e( 'You must pick a font','taxonomy_icon_font' ); ?>" name="icon" id="icons" class="form-control by_class font-icon-picker">
			    <option value="" selected="selected"><?php _e( '--Please Select--.','taxonomy_icon_font' ); ?></option>
			    <?php echo imii_generate_select_options( $this->icomoon_icons(), 'class' ); ?>
			</select>

			<p class="description"><?php _e( 'Select an icon that will be connected to the taxonomy.','taxonomy_icon_font' ); ?></p>

		</div>

		<?php
	}

	public function icomoon_icons(){

		$icomoon_icons = array(
			array(
				'id' => 'web_elements',
				'label' => 'Web Applications',
				'elements' => array(
					0xe851 => 'Account box',  0xe05c => 'Add to queue',  0xe2bc => 'Attachment',  0xe2c3 => 'Cloud upload',  0xe0b4 => 'Call missed',  0xe0b5 => 'Call received',
					0xe0b6 => 'Call split',  0xe0b7 => 'Chat',  0xe0ca => 'Chat bubble',  0xe834 => 'Check box',  0xe835 => 'Check box outline blank',  0xe0b8 => 'Clear all',
					0xe01c => 'Closed caption',  0xe2c0 => 'Cloud download',  0xe86f => 'Code',  0xe431 => 'Collections bookmark',  0xe0bb => 'Dialer sip',  0xe0bc => 'Dialpad',
					0xe15c => 'Remove circle',  0xe01e => 'Explicit',  0xe05d => 'Fiber dvr',  0xe05e => 'Fiber new',  0xe2c6 => 'File upload',  0xe241 => 'Format list bulleted',
					0xe242 => 'Format list numbered',  0xe244 => 'Format quote',  0xe909 => 'Indeterminate check box',  0xe3f9 => 'Leak remove',  0xe896 => 'List',  0xe561 => 'Restaurant menu',
					0xe5d2 => 'Menu',  0xe0c9 => 'Message',  0xe90a => 'Offline pin',  0xe55a => 'Person pin',  0xe56a => 'Person pin circle',  0xe55e => 'Pin drop',
					0xe05f => 'Playlist play',  0xe8ac => 'Power settings new',  0xe15b => 'Remove',  0xe928 => 'Remove shopping cart',  0xe041 => 'Repeat one',  0xe8b6 => 'Search',
					0xe8b8 => 'Settings',  0xe8b9 => 'Settings applications',  0xe8ba => 'Settings backup restore',  0xe8bb => 'Settings bluetooth',  0xe8bd => 'Settings brightness',  0xe8bc => 'Settings cell',
					0xe8be => 'Settings ethernet',  0xe8bf => 'Settings input antenna',  0xe8c1 => 'Settings input composite',  0xe8c2 => 'Settings input hdmi',  0xe8c3 => 'Settings input svideo',  0xe8c4 => 'Settings overscan',
					0xe8c5 => 'Settings phone',  0xe8c6 => 'Settings power',  0xe8c7 => 'Settings remote',  0xe1c3 => 'Settings system daydream',  0xe8c8 => 'Settings voice',  0xe8ef => 'View list',

				),
				'element_classes' => array(
					0xe851 => 'icon-hipster-account_box',  0xe05c => 'icon-hipster-add_to_queue',  0xe2bc => 'icon-hipster-attachment',  0xe2c3 => 'icon-hipster-cloud_upload',  0xe0b4 => 'icon-hipster-call_missed',  0xe0b5 => 'icon-hipster-call_received',
					0xe0b6 => 'icon-hipster-call_split',  0xe0b7 => 'icon-hipster-chat',  0xe0ca => 'icon-hipster-chat_bubble',  0xe834 => 'icon-hipster-check_box',  0xe835 => 'icon-hipster-check_box_outline_blank',  0xe0b8 => 'icon-hipster-clear_all',
					0xe01c => 'icon-hipster-closed_caption',  0xe2c0 => 'icon-hipster-cloud_download',  0xe86f => 'icon-hipster-code',  0xe431 => 'icon-hipster-collections_bookmark',  0xe0bb => 'icon-hipster-dialer_sip',  0xe0bc => 'icon-hipster-dialpad',
					0xe15c => 'icon-hipster-remove_circle',  0xe01e => 'icon-hipster-explicit',  0xe05d => 'icon-hipster-fiber_dvr',  0xe05e => 'icon-hipster-fiber_new',  0xe2c6 => 'icon-hipster-file_upload',  0xe241 => 'icon-hipster-format_list_bulleted',
					0xe242 => 'icon-hipster-format_list_numbered',  0xe244 => 'icon-hipster-format_quote',  0xe909 => 'icon-hipster-indeterminate_check_box',  0xe3f9 => 'icon-hipster-leak_remove',  0xe896 => 'icon-hipster-list',  0xe561 => 'icon-hipster-restaurant_menu',
					0xe5d2 => 'icon-hipster-menu',  0xe0c9 => 'icon-hipster-message',  0xe90a => 'icon-hipster-offline_pin',  0xe55a => 'icon-hipster-person_pin',  0xe56a => 'icon-hipster-person_pin_circle',  0xe55e => 'icon-hipster-pin_drop',
					0xe05f => 'icon-hipster-playlist_play',  0xe8ac => 'icon-hipster-power_settings_new',  0xe15b => 'icon-hipster-remove',  0xe928 => 'icon-hipster-remove_shopping_cart',  0xe041 => 'icon-hipster-repeat_one',  0xe8b6 => 'icon-hipster-search',
					0xe8b8 => 'icon-hipster-settings',  0xe8b9 => 'icon-hipster-settings_applications',  0xe8ba => 'icon-hipster-settings_backup_restore',  0xe8bb => 'icon-hipster-settings_bluetooth',  0xe8bd => 'icon-hipster-settings_brightness',  0xe8bc => 'icon-hipster-settings_cell',
					0xe8be => 'icon-hipster-settings_ethernet',  0xe8bf => 'icon-hipster-settings_input_antenna',  0xe8c1 => 'icon-hipster-settings_input_composite',  0xe8c2 => 'icon-hipster-settings_input_hdmi',  0xe8c3 => 'icon-hipster-settings_input_svideo',  0xe8c4 => 'icon-hipster-settings_overscan',
					0xe8c5 => 'icon-hipster-settings_phone',  0xe8c6 => 'icon-hipster-settings_power',  0xe8c7 => 'icon-hipster-settings_remote',  0xe1c3 => 'icon-hipster-settings_system_daydream',  0xe8c8 => 'icon-hipster-settings_voice',  0xe8ef => 'icon-hipster-view_list',

				),
				'tags' => array(
					'list', 'download', 'upload', 'www', 'globe', 'code', 'embed', 'bookmark', 'attachment', 'box', 'menu', 'feed', 'pin', 'quote', 'search', 'settings', 'remove', 'load', 'bookmarks',
				),
			),
			array(
				'id' => 'spinners',
				'label' => 'Spinners',
				'elements' => array(
					0xe615 => 'Event busy',
				),
				'element_classes' => array(
					0xe615 => 'icon-hipster-event_busy',
				),
				'tags' => array(
					'spinner', 'spinners', 'loading', 'wait', 'ajax', 'busy',
				),
			),
			array(
				'id' => 'business',
				'label' => 'Business Icons',
				'elements' => array(
					0xe003 => 'Add alert',  0xe06b => 'Branding watermark',  0xe6dd => 'Bubble chart',  0xe06c => 'Call to action',  0xe413 => 'Photo library',  0xe01f => 'Fast forward',
					0xe020 => 'Fast rewind',  0xe06d => 'Featured play list',  0xe06e => 'Featured video',  0xe06a => 'Fiber pin',  0xe021 => 'Games',  0xe7f0 => 'Group add',
					0xe886 => 'Group work',  0xe023 => 'Hearing',  0xe24c => 'Insert comment',  0xe54b => 'Local library',  0xe253 => 'Mode comment',  0xe06f => 'Note',
					0xe906 => 'Play for work',  0xe03b => 'Playlist add',  0xe03d => 'Queue music',  0xe03e => 'Radio',  0xe03f => 'Recent actors',  0xe15f => 'Reply all',
					0xe160 => 'Report',  0xe161 => 'Save',  0xe162 => 'Select all',  0xe32e => 'Speaker group',  0xe8e8 => 'Verified user',  0xe070 => 'Video call',
					0xe071 => 'Video label',  0xe8f9 => 'Work',
				),
				'element_classes' => array(
					0xe003 => 'icon-hipster-add_alert',  0xe06b => 'icon-hipster-branding_watermark',  0xe6dd => 'icon-hipster-bubble_chart',  0xe06c => 'icon-hipster-call_to_action',  0xe413 => 'icon-hipster-photo_library',  0xe01f => 'icon-hipster-fast_forward',
					0xe020 => 'icon-hipster-fast_rewind',  0xe06d => 'icon-hipster-featured_play_list',  0xe06e => 'icon-hipster-featured_video',  0xe06a => 'icon-hipster-fiber_pin',  0xe021 => 'icon-hipster-games',  0xe7f0 => 'icon-hipster-group_add',
					0xe886 => 'icon-hipster-group_work',  0xe023 => 'icon-hipster-hearing',  0xe24c => 'icon-hipster-insert_comment',  0xe54b => 'icon-hipster-local_library',  0xe253 => 'icon-hipster-mode_comment',  0xe06f => 'icon-hipster-note',
					0xe906 => 'icon-hipster-play_for_work',  0xe03b => 'icon-hipster-playlist_add',  0xe03d => 'icon-hipster-queue_music',  0xe03e => 'icon-hipster-radio',  0xe03f => 'icon-hipster-recent_actors',  0xe15f => 'icon-hipster-reply_all',
					0xe160 => 'icon-hipster-report',  0xe161 => 'icon-hipster-save',  0xe162 => 'icon-hipster-select_all',  0xe32e => 'icon-hipster-speaker_group',  0xe8e8 => 'icon-hipster-verified_user',  0xe070 => 'icon-hipster-video_call',
					0xe071 => 'icon-hipster-video_label',  0xe8f9 => 'icon-hipster-work',
				),
				'tags' => array(
					'support', 'testimonial', 'user', 'comment', 'member', 'office', 'newspaper', 'book', 'books', 'library', 'profile', 'bubble', 'quote', 'bubbles', 'comments', 'group', 'team', 'community', 'work', 'job', 'signup', 'login', 'register', 'signout', 'logout',
				),
			),
			array(
				'id' => 'ecommerce',
				'label' => 'eCommerce',
				'elements' => array(
					0xe854 => 'Add shopping cart',  0xe8f7 => 'Card membership',  0xe8f8 => 'Card travel',  0xe420 => 'Tag faces',  0xe030 => 'Library music',  0xe8cc => 'Shopping cart',
					0xe031 => 'New releases',  0xe035 => 'Pause circle filled',  0xe036 => 'Pause circle outline',  0xe037 => 'Play arrow',  0xe039 => 'Play circle outline',  0xe32b => 'Sim card',
					0xe624 => 'Sim card alert',
				),
				'element_classes' => array(
					0xe854 => 'icon-hipster-add_shopping_cart',  0xe8f7 => 'icon-hipster-card_membership',  0xe8f8 => 'icon-hipster-card_travel',  0xe420 => 'icon-hipster-tag_faces',  0xe030 => 'icon-hipster-library_music',  0xe8cc => 'icon-hipster-shopping_cart',
					0xe031 => 'icon-hipster-new_releases',  0xe035 => 'icon-hipster-pause_circle_filled',  0xe036 => 'icon-hipster-pause_circle_outline',  0xe037 => 'icon-hipster-play_arrow',  0xe039 => 'icon-hipster-play_circle_outline',  0xe32b => 'icon-hipster-sim_card',
					0xe624 => 'icon-hipster-sim_card_alert',
				),
				'tags' => array(
					'cart', 'card', 'calculator', 'tag', 'tags', 'calculate',
				),
			),
			array(
				'id' => 'currency',
				'label' => 'Currency Icons',
				'elements' => array(
					0xe227 => 'Attach money',  0xe926 => 'Euro symbol',  0xe25c => 'Money off',  0xe038 => 'Play circle filled',
				),
				'element_classes' => array(
					0xe227 => 'icon-hipster-attach_money',  0xe926 => 'icon-hipster-euro_symbol',  0xe25c => 'icon-hipster-money_off',  0xe038 => 'icon-hipster-play_circle_filled',
				),
				'tags' => array(
					'btc', 'bitcoin', 'cny', 'dollar', 'eur', 'euro', 'gbp', 'inr', 'jpy', 'krw', 'money', 'rmb', 'rouble', 'rub', 'ruble', 'rupee', 'try', 'turkish lira', 'usd', 'won', 'yen',
				),
			),
			array(
				'id' => 'form_controls',
				'label' => 'Form Control Icons',
				'elements' => array(
					0xe060 => 'Art track',  0xe5ca => 'Check',  0xe86c => 'Check circle',  0xe01d => 'Equalizer',  0xe879 => 'Exit to app',  0xe061 => 'Fiber manual record',
					0xe5d1 => 'Fullscreen exit',  0xe029 => 'Mic',  0xe02a => 'Mic none',  0xe02b => 'Mic off',  0xe640 => 'Network check',  0xe836 => 'Radio button unchecked',
					0xe837 => 'Radio button checked',
				),
				'element_classes' => array(
					0xe060 => 'icon-hipster-art_track',  0xe5ca => 'icon-hipster-check',  0xe86c => 'icon-hipster-check_circle',  0xe01d => 'icon-hipster-equalizer',  0xe879 => 'icon-hipster-exit_to_app',  0xe061 => 'icon-hipster-fiber_manual_record',
					0xe5d1 => 'icon-hipster-fullscreen_exit',  0xe029 => 'icon-hipster-mic',  0xe02a => 'icon-hipster-mic_none',  0xe02b => 'icon-hipster-mic_off',  0xe640 => 'icon-hipster-network_check',  0xe836 => 'icon-hipster-radio_button_unchecked',
					0xe837 => 'icon-hipster-radio_button_checked',
				),
				'tags' => array(
					'cut', 'copy', 'paste', 'enter', 'exit', 'save', 'trash', 'check', 'radio', 'checked', 'selected',
				),
			),
			array(
				'id' => 'text_editor',
				'label' => 'User Action & Text Editor',
				'elements' => array(
					0xe145 => 'Add',  0xe146 => 'Add box',  0xe147 => 'Add circle',  0xe149 => 'Archive',  0xe153 => 'Flag',  0xe14a => 'Backspace',
					0xe14b => 'Block',  0xe14d => 'Content copy',  0xe14e => 'Content cut',  0xe14f => 'Content paste',  0xe437 => 'Crop rotate',  0xe151 => 'Drafts',
					0xe159 => 'Markunread',  0xe5ce => 'Expand less',  0xe5cf => 'Expand more',  0xe062 => 'Fiber smart record',  0xe152 => 'Filter list',  0xe3e8 => 'Flip',
					0xe882 => 'Flip to back',  0xe883 => 'Flip to front',  0xe234 => 'Format align center',  0xe235 => 'Format align justify',  0xe236 => 'Format align left',  0xe237 => 'Format align right',
					0xe238 => 'Format bold',  0xe23d => 'Format indent decrease',  0xe23e => 'Format indent increase',  0xe23f => 'Format italic',  0xe246 => 'Format strikethrough',  0xe154 => 'Forward',
					0xe155 => 'Gesture',  0xe0c3 => 'Import export',  0xe156 => 'Inbox',  0xe157 => 'Link',  0xe063 => 'Music video',  0xe065 => 'Playlist add check',
					0xe066 => 'Queue play next',  0xe15a => 'Redo',  0xe067 => 'Remove from queue',  0xe418 => 'Rotate  90',  0xe419 => 'Rotate left',  0xe41a => 'Rotate right',
					0xe257 => 'Strikethrough s',  0xe064 => 'Subscriptions',  0xe258 => 'Vertical align bottom',  0xe259 => 'Vertical align center',  0xe25a => 'Vertical align top',
				),
				'element_classes' => array(
					0xe145 => 'icon-hipster-add',  0xe146 => 'icon-hipster-add_box',  0xe147 => 'icon-hipster-add_circle',  0xe149 => 'icon-hipster-archive',  0xe153 => 'icon-hipster-flag',  0xe14a => 'icon-hipster-backspace',
					0xe14b => 'icon-hipster-block',  0xe14d => 'icon-hipster-content_copy',  0xe14e => 'icon-hipster-content_cut',  0xe14f => 'icon-hipster-content_paste',  0xe437 => 'icon-hipster-crop_rotate',  0xe151 => 'icon-hipster-drafts',
					0xe159 => 'icon-hipster-markunread',  0xe5ce => 'icon-hipster-expand_less',  0xe5cf => 'icon-hipster-expand_more',  0xe062 => 'icon-hipster-fiber_smart_record',  0xe152 => 'icon-hipster-filter_list',  0xe3e8 => 'icon-hipster-flip',
					0xe882 => 'icon-hipster-flip_to_back',  0xe883 => 'icon-hipster-flip_to_front',  0xe234 => 'icon-hipster-format_align_center',  0xe235 => 'icon-hipster-format_align_justify',  0xe236 => 'icon-hipster-format_align_left',  0xe237 => 'icon-hipster-format_align_right',
					0xe238 => 'icon-hipster-format_bold',  0xe23d => 'icon-hipster-format_indent_decrease',  0xe23e => 'icon-hipster-format_indent_increase',  0xe23f => 'icon-hipster-format_italic',  0xe246 => 'icon-hipster-format_strikethrough',  0xe154 => 'icon-hipster-forward',
					0xe155 => 'icon-hipster-gesture',  0xe0c3 => 'icon-hipster-import_export',  0xe156 => 'icon-hipster-inbox',  0xe157 => 'icon-hipster-link',  0xe063 => 'icon-hipster-music_video',  0xe065 => 'icon-hipster-playlist_add_check',
					0xe066 => 'icon-hipster-queue_play_next',  0xe15a => 'icon-hipster-redo',  0xe067 => 'icon-hipster-remove_from_queue',  0xe418 => 'icon-hipster-rotate_90_degrees_ccw',  0xe419 => 'icon-hipster-rotate_left',  0xe41a => 'icon-hipster-rotate_right',
					0xe257 => 'icon-hipster-strikethrough_s',  0xe064 => 'icon-hipster-subscriptions',  0xe258 => 'icon-hipster-vertical_align_bottom',  0xe259 => 'icon-hipster-vertical_align_center',  0xe25a => 'icon-hipster-vertical_align_top',
				),
				'tags' => array(
					'bold', 'underline', 'italic', 'strike', 'align', 'indent', 'anchor', 'table', 'chain', 'floppy', 'list', 'outdent', 'paperclip', 'rotate', 'scissors', 'strikethrough', 'th', 'undo', 'unlink', 'clipboard', 'flip', 'redo', 'zoomin', 'zoomout', 'expand', 'contract', 'link', 'font', 'wysiwyg',
				),
			),
			array(
				'id' => 'charts',
				'label' => 'Charts and Codes',
				'elements' => array(
					0xe033 => 'Not interested',  0xe240 => 'Format line spacing',  0xe919 => 'Line style',  0xe91a => 'Line weight',  0xe6df => 'Multiline chart',  0xe6c4 => 'Pie chart',
					0xe6c5 => 'Pie chart outlined',  0xe6e1 => 'Show chart',  0xe8ec => 'View column',
				),
				'element_classes' => array(
					0xe033 => 'icon-hipster-not_interested',  0xe240 => 'icon-hipster-format_line_spacing',  0xe919 => 'icon-hipster-line_style',  0xe91a => 'icon-hipster-line_weight',  0xe6df => 'icon-hipster-multiline_chart',  0xe6c4 => 'icon-hipster-pie_chart',
					0xe6c5 => 'icon-hipster-pie_chart_outlined',  0xe6e1 => 'icon-hipster-show_chart',  0xe8ec => 'icon-hipster-view_column',
				),
				'tags' => array(
					'pie', 'line', 'qrcode', 'column', 'barcode', 'bars', 'chart', 'charts', 'graph',
				),
			),
			array(
				'id' => 'attentive',
				'label' => 'Attentive',
				'elements' => array(
					0xe5c9 => 'Cancel',  0xe8af => 'Question answer',  0xe887 => 'Help',  0xe8fd => 'Help outline',  0xe88e => 'Info',  0xe88f => 'Info outline',
					0xe0c7 => 'Location off',
				),
				'element_classes' => array(
					0xe5c9 => 'icon-hipster-cancel',  0xe8af => 'icon-hipster-question_answer',  0xe887 => 'icon-hipster-help',  0xe8fd => 'icon-hipster-help_outline',  0xe88e => 'icon-hipster-info',  0xe88f => 'icon-hipster-info_outline',
					0xe0c7 => 'icon-hipster-location_off',
				),
				'tags' => array(
					'help', 'warning', 'info', 'blocked', 'cancel', 'question', 'spam', 'block', 'allowed', 'not allowed', 'okay', 'cancel',
				),
			),
			array(
				'id' => 'multimedia',
				'label' => 'Multimedia Icons',
				'elements' => array(
					0xe439 => 'Add a photo',  0xe5c8 => 'Arrow forward',  0xe3a6 => 'Brightness  1',  0xe3a7 => 'Brightness  2',  0xe3a8 => 'Brightness  3',  0xe3a9 => 'Brightness  4',
					0xe3ad => 'Broken image',  0xe0cd => 'Phone',  0xe412 => 'Photo camera',  0xe409 => 'Navigate next',  0xe410 => 'Photo',  0xe3f5 => 'Image aspect ratio',
					0xe553 => 'Local play',  0xe403 => 'Monochrome photos',  0xe405 => 'Music note',  0xe63a => 'Ondemand video',  0xe8a7 => 'Perm media',  0xe63b => 'Personal video',
					0xe411 => 'Photo album',  0xe43b => 'Photo filter',  0xe432 => 'Photo size select actual',  0xe433 => 'Photo size select large',  0xe434 => 'Photo size select small',  0xe415 => 'Picture as pdf',
					0xe8aa => 'Picture in picture',  0xe911 => 'Picture in picture alt',  0xe068 => 'Slow motion video',  0xe41f => 'Switch video',  0xe62b => 'Tap and play',
				),
				'element_classes' => array(
					0xe439 => 'icon-hipster-add_a_photo',  0xe5c8 => 'icon-hipster-arrow_forward',  0xe3a6 => 'icon-hipster-brightness_1',  0xe3a7 => 'icon-hipster-brightness_2',  0xe3a8 => 'icon-hipster-brightness_3',  0xe3a9 => 'icon-hipster-brightness_4',
					0xe3ad => 'icon-hipster-broken_image',  0xe0cd => 'icon-hipster-phone',  0xe412 => 'icon-hipster-photo_camera',  0xe409 => 'icon-hipster-navigate_next',  0xe410 => 'icon-hipster-photo',  0xe3f5 => 'icon-hipster-image_aspect_ratio',
					0xe553 => 'icon-hipster-local_play',  0xe403 => 'icon-hipster-monochrome_photos',  0xe405 => 'icon-hipster-music_note',  0xe63a => 'icon-hipster-ondemand_video',  0xe8a7 => 'icon-hipster-perm_media',  0xe63b => 'icon-hipster-personal_video',
					0xe411 => 'icon-hipster-photo_album',  0xe43b => 'icon-hipster-photo_filter',  0xe432 => 'icon-hipster-photo_size_select_actual',  0xe433 => 'icon-hipster-photo_size_select_large',  0xe434 => 'icon-hipster-photo_size_select_small',  0xe415 => 'icon-hipster-picture_as_pdf',
					0xe8aa => 'icon-hipster-picture_in_picture',  0xe911 => 'icon-hipster-picture_in_picture_alt',  0xe068 => 'icon-hipster-slow_motion_video',  0xe41f => 'icon-hipster-switch_video',  0xe62b => 'icon-hipster-tap_and_play',
				),
				'tags' => array(
					'image', 'images', 'picture', 'pictures', 'photo', 'photos', 'gallery', 'video', 'fast forward', 'forward', 'rewind', 'fast rewind', 'volume', 'mute', 'pause', 'equalizer', 'next', 'previous', 'brightness', 'contrast', 'play', 'film', 'music', 'media', 'media control',
				),
			),
			array(
				'id' => 'location',
				'label' => 'Location and Contact',
				'elements' => array(
					0xe567 => 'Add location',  0xe03c => 'Queue',  0xe055 => 'Airplay',  0xe568 => 'Edit location',  0xe000 => 'Error',  0xe001 => 'Error outline',
					0xe55c => 'My location',  0xe88a => 'Home',  0xe0c4 => 'Invert colors off',  0xe7f1 => 'Location city',  0xe55b => 'Map',  0xe8a8 => 'Perm phone msg',
					0xe324 => 'Phone android',  0xe61b => 'Phone bluetooth speaker',  0xe61c => 'Phone forwarded',  0xe61d => 'Phone in talk',  0xe325 => 'Phone iphone',  0xe61e => 'Phone locked',
					0xe61f => 'Phone missed',  0xe620 => 'Phone paused',  0xe040 => 'Repeat',  0xe042 => 'Replay',  0xe002 => 'Warning',  0xe043 => 'Shuffle',
					0xe045 => 'Skip previous',  0xe046 => 'Snooze',  0xe56b => 'Zoom out map',
				),
				'element_classes' => array(
					0xe567 => 'icon-hipster-add_location',  0xe03c => 'icon-hipster-queue',  0xe055 => 'icon-hipster-airplay',  0xe568 => 'icon-hipster-edit_location',  0xe000 => 'icon-hipster-error',  0xe001 => 'icon-hipster-error_outline',
					0xe55c => 'icon-hipster-my_location',  0xe88a => 'icon-hipster-home',  0xe0c4 => 'icon-hipster-invert_colors_off',  0xe7f1 => 'icon-hipster-location_city',  0xe55b => 'icon-hipster-map',  0xe8a8 => 'icon-hipster-perm_phone_msg',
					0xe324 => 'icon-hipster-phone_android',  0xe61b => 'icon-hipster-phone_bluetooth_speaker',  0xe61c => 'icon-hipster-phone_forwarded',  0xe61d => 'icon-hipster-phone_in_talk',  0xe325 => 'icon-hipster-phone_iphone',  0xe61e => 'icon-hipster-phone_locked',
					0xe61f => 'icon-hipster-phone_missed',  0xe620 => 'icon-hipster-phone_paused',  0xe040 => 'icon-hipster-repeat',  0xe042 => 'icon-hipster-replay',  0xe002 => 'icon-hipster-warning',  0xe043 => 'icon-hipster-shuffle',
					0xe045 => 'icon-hipster-skip_previous',  0xe046 => 'icon-hipster-snooze',  0xe56b => 'icon-hipster-zoom_out_map',
				),
				'tags' => array(
					'home', 'map', 'phone', 'phone book', 'address', 'address book', 'marker', 'location', 'email', 'envelope', 'flag',
				),
			),
			array(
				'id' => 'datetime',
				'label' => 'Date and Time',
				'elements' => array(
					0xe86b => 'Change history',  0xe8a3 => 'Perm contact calendar',  0xe047 => 'Stop',  0xe048 => 'Subtitles',  0xe049 => 'Surround sound',  0xe04a => 'Video library',
					0xe04b => 'Videocam',  0xe04d => 'Volume down',  0xe04e => 'Volume mute',  0xe04f => 'Volume off',  0xe334 => 'Watch',  0xe924 => 'Watch later',

				),
				'element_classes' => array(
					0xe86b => 'icon-hipster-change_history',  0xe8a3 => 'icon-hipster-perm_contact_calendar',  0xe047 => 'icon-hipster-stop',  0xe048 => 'icon-hipster-subtitles',  0xe049 => 'icon-hipster-surround_sound',  0xe04a => 'icon-hipster-video_library',
					0xe04b => 'icon-hipster-videocam',  0xe04d => 'icon-hipster-volume_down',  0xe04e => 'icon-hipster-volume_mute',  0xe04f => 'icon-hipster-volume_off',  0xe334 => 'icon-hipster-watch',  0xe924 => 'icon-hipster-watch_later',

				),
				'tags' => array(
					'clock', 'calendar', 'month', 'year', 'history', 'stopwatch', 'watch',
				),
			),
			array(
				'id' => 'devices',
				'label' => 'Devices',
				'elements' => array(
					0xe30b => 'Desktop mac',  0xe30c => 'Desktop windows',  0xe056 => 'Forward  10',  0xe057 => 'Forward  30',  0xe052 => 'Hd',  0xe312 => 'Keyboard',
					0xe313 => 'Keyboard arrow down',  0xe314 => 'Keyboard arrow left',  0xe315 => 'Keyboard arrow right',  0xe316 => 'Keyboard arrow up',  0xe317 => 'Keyboard backspace',  0xe318 => 'Keyboard capslock',
					0xe31a => 'Keyboard hide',  0xe31b => 'Keyboard return',  0xe31c => 'Keyboard tab',  0xe31d => 'Keyboard voice',  0xe323 => 'Mouse',  0xe053 => 'Sort by alpha',
					0xe32f => 'Tablet',  0xe330 => 'Tablet android',  0xe331 => 'Tablet mac',  0xe050 => 'Volume up',  0xe051 => 'Web',
				),
				'element_classes' => array(
					0xe30b => 'icon-hipster-desktop_mac',  0xe30c => 'icon-hipster-desktop_windows',  0xe056 => 'icon-hipster-forward_10',  0xe057 => 'icon-hipster-forward_30',  0xe052 => 'icon-hipster-hd',  0xe312 => 'icon-hipster-keyboard',
					0xe313 => 'icon-hipster-keyboard_arrow_down',  0xe314 => 'icon-hipster-keyboard_arrow_left',  0xe315 => 'icon-hipster-keyboard_arrow_right',  0xe316 => 'icon-hipster-keyboard_arrow_up',  0xe317 => 'icon-hipster-keyboard_backspace',  0xe318 => 'icon-hipster-keyboard_capslock',
					0xe31a => 'icon-hipster-keyboard_hide',  0xe31b => 'icon-hipster-keyboard_return',  0xe31c => 'icon-hipster-keyboard_tab',  0xe31d => 'icon-hipster-keyboard_voice',  0xe323 => 'icon-hipster-mouse',  0xe053 => 'icon-hipster-sort_by_alpha',
					0xe32f => 'icon-hipster-tablet',  0xe330 => 'icon-hipster-tablet_android',  0xe331 => 'icon-hipster-tablet_mac',  0xe050 => 'icon-hipster-volume_up',  0xe051 => 'icon-hipster-web',
				),
				'tags' => array(
					'printer', 'keyboard', 'mouse', 'phone', 'tablet', 'tabloid', 'desktop', 'computer', 'imac', 'television', 'monitor', 'screen',
				),
			),
			array(
				'id' => 'tools',
				'label' => 'Tools',
				'elements' => array(
					0xe3ae => 'Brush',  0xe3d3 => 'Filter',  0xe3d0 => 'Filter  1',  0xe3d1 => 'Filter  2',  0xe3d2 => 'Filter  3',  0xe3d4 => 'Filter  4',
					0xe3d5 => 'Filter  5',  0xe3d6 => 'Filter  6',  0xe3d7 => 'Filter  7',  0xe3d8 => 'Filter  8',  0xe3d9 => 'Filter  9',  0xe3da => 'Filter  9',
					0xe3db => 'Filter b and w',  0xe3dc => 'Filter center focus',  0xe3dd => 'Filter drama',  0xe3de => 'Filter frames',  0xe3e0 => 'Filter none',  0xe3e2 => 'Filter tilt shift',
					0xe3e3 => 'Filter vintage',  0xe243 => 'Format paint',  0xe897 => 'Lock',  0xe898 => 'Lock open',  0xe899 => 'Lock outline',  0xe43a => 'Movie filter',
					0xe925 => 'Pan tool',  0xe62f => 'Vpn lock',  0xe1e1 => 'Wifi lock',
				),
				'element_classes' => array(
					0xe3ae => 'icon-hipster-brush',  0xe3d3 => 'icon-hipster-filter',  0xe3d0 => 'icon-hipster-filter_1',  0xe3d1 => 'icon-hipster-filter_2',  0xe3d2 => 'icon-hipster-filter_3',  0xe3d4 => 'icon-hipster-filter_4',
					0xe3d5 => 'icon-hipster-filter_5',  0xe3d6 => 'icon-hipster-filter_6',  0xe3d7 => 'icon-hipster-filter_7',  0xe3d8 => 'icon-hipster-filter_8',  0xe3d9 => 'icon-hipster-filter_9',  0xe3da => 'icon-hipster-filter_9_plus',
					0xe3db => 'icon-hipster-filter_b_and_w',  0xe3dc => 'icon-hipster-filter_center_focus',  0xe3dd => 'icon-hipster-filter_drama',  0xe3de => 'icon-hipster-filter_frames',  0xe3e0 => 'icon-hipster-filter_none',  0xe3e2 => 'icon-hipster-filter_tilt_shift',
					0xe3e3 => 'icon-hipster-filter_vintage',  0xe243 => 'icon-hipster-format_paint',  0xe897 => 'icon-hipster-lock',  0xe898 => 'icon-hipster-lock_open',  0xe899 => 'icon-hipster-lock_outline',  0xe43a => 'icon-hipster-movie_filter',
					0xe925 => 'icon-hipster-pan_tool',  0xe62f => 'icon-hipster-vpn_lock',  0xe1e1 => 'icon-hipster-wifi_lock',
				),
				'tags' => array(
					'wrench', 'cog', 'cogs', 'pen', 'pencil', 'key', 'lock', 'unlock', 'unlocked', 'filter', 'brush', 'paint', 'dice', 'quill', 'paint-format', 'tool', 'hammer', 'magnet',
				),
			),
			array(
				'id' => 'social',
				'label' => 'Social Networking',
				'elements' => array(
					0xe191 => 'Access alarms',  0xe194 => 'Airplanemode inactive',  0xe19c => 'Battery alert',  0xe1a3 => 'Battery charging full',  0xe1a5 => 'Battery std',  0xe1a6 => 'Battery unknown',
					0xe1a7 => 'Bluetooth',  0xe1aa => 'Bluetooth searching',  0xe1a8 => 'Bluetooth connected',  0xe1a9 => 'Bluetooth disabled',  0xe1ac => 'Brightness high',  0xe1ab => 'Brightness auto',
					0xe16c => 'Delete sweep',  0xe1b0 => 'Developer mode',  0xe167 => 'Font download',  0xe24d => 'Insert drive file',  0xe16d => 'Low priority',  0xe168 => 'Move to inbox',
					0xe16a => 'Next week',  0xe15e => 'Reply',  0xe80d => 'Share',  0xe164 => 'Sort',  0xe165 => 'Text format',  0xe169 => 'Unarchive',
					0xe166 => 'Undo',  0xe16b => 'Weekend',
				),
				'element_classes' => array(
					0xe191 => 'icon-hipster-access_alarms',  0xe194 => 'icon-hipster-airplanemode_inactive',  0xe19c => 'icon-hipster-battery_alert',  0xe1a3 => 'icon-hipster-battery_charging_full',  0xe1a5 => 'icon-hipster-battery_std',  0xe1a6 => 'icon-hipster-battery_unknown',
					0xe1a7 => 'icon-hipster-bluetooth',  0xe1aa => 'icon-hipster-bluetooth_searching',  0xe1a8 => 'icon-hipster-bluetooth_connected',  0xe1a9 => 'icon-hipster-bluetooth_disabled',  0xe1ac => 'icon-hipster-brightness_high',  0xe1ab => 'icon-hipster-brightness_auto',
					0xe16c => 'icon-hipster-delete_sweep',  0xe1b0 => 'icon-hipster-developer_mode',  0xe167 => 'icon-hipster-font_download',  0xe24d => 'icon-hipster-insert_drive_file',  0xe16d => 'icon-hipster-low_priority',  0xe168 => 'icon-hipster-move_to_inbox',
					0xe16a => 'icon-hipster-next_week',  0xe15e => 'icon-hipster-reply',  0xe80d => 'icon-hipster-share',  0xe164 => 'icon-hipster-sort',  0xe165 => 'icon-hipster-text_format',  0xe169 => 'icon-hipster-unarchive',
					0xe166 => 'icon-hipster-undo',  0xe16b => 'icon-hipster-weekend',
				),
				'tags' => array(
					'share', 'google plus', 'googleplus', 'google drive', 'drive', 'facebook', 'twitter', 'vimeo', 'picasa', 'social', 'github', 'wordpress', 'pinterest', 'tumblr', 'yahoo', 'lastfm', 'linkedin', 'stumbleupon', 'soundcloud', 'reddit', 'yelp', 'stackoverflow',
				),
			),
			array(
				'id' => 'brands',
				'label' => 'Brands Icons',
				'elements' => array(
					0xe859 => 'Android',  0xe1ad => 'Brightness low',  0xe1ae => 'Brightness medium',  0xe86d => 'Chrome reader mode',  0xe1af => 'Data usage',  0xe321 => 'Laptop windows',
					0xe1ba => 'Network wifi',  0xe1bb => 'Nfc',  0xe89d => 'Open in browser',  0xe1be => 'Screen lock landscape',  0xe1bf => 'Screen lock portrait',  0xe1c0 => 'Screen lock rotation',
					0xe1c1 => 'Screen rotation',  0xe1bc => 'Wallpaper',  0xe1bd => 'Widgets',
				),
				'element_classes' => array(
					0xe859 => 'icon-hipster-android',  0xe1ad => 'icon-hipster-brightness_low',  0xe1ae => 'icon-hipster-brightness_medium',  0xe86d => 'icon-hipster-chrome_reader_mode',  0xe1af => 'icon-hipster-data_usage',  0xe321 => 'icon-hipster-laptop_windows',
					0xe1ba => 'icon-hipster-network_wifi',  0xe1bb => 'icon-hipster-nfc',  0xe89d => 'icon-hipster-open_in_browser',  0xe1be => 'icon-hipster-screen_lock_landscape',  0xe1bf => 'icon-hipster-screen_lock_portrait',  0xe1c0 => 'icon-hipster-screen_lock_rotation',
					0xe1c1 => 'icon-hipster-screen_rotation',  0xe1bc => 'icon-hipster-wallpaper',  0xe1bd => 'icon-hipster-widgets',
				),
				'tags' => array(
					'apple', 'android', 'paypal', 'linux', 'finder', 'windows', 'skype', 'chrome', 'firefox', 'explorer', 'safari', 'opera', 'joomla', 'browser', 'browsers', 'html5', 'css3',
				),
			),
			array(
				'id' => 'documents',
				'label' => 'Files & Documents',
				'elements' => array(
					0xe226 => 'Attach file',  0xe2cc => 'Create new folder',  0xe1b2 => 'Dvr',  0xe2c7 => 'Folder',  0xe2c8 => 'Folder open',  0xe2c9 => 'Folder shared',
					0xe617 => 'Folder special',  0xe058 => 'Forward  5',  0xe1b7 => 'Location searching',  0xe1b6 => 'Location disabled',  0xe1b8 => 'Graphic eq',  0xe024 => 'High quality',
					0xe02f => 'Library books',  0xe1b9 => 'Network cell',  0xe059 => 'Replay  10',  0xe05a => 'Replay  30',  0xe05b => 'Replay  5',
				),
				'element_classes' => array(
					0xe226 => 'icon-hipster-attach_file',  0xe2cc => 'icon-hipster-create_new_folder',  0xe1b2 => 'icon-hipster-dvr',  0xe2c7 => 'icon-hipster-folder',  0xe2c8 => 'icon-hipster-folder_open',  0xe2c9 => 'icon-hipster-folder_shared',
					0xe617 => 'icon-hipster-folder_special',  0xe058 => 'icon-hipster-forward_5',  0xe1b7 => 'icon-hipster-location_searching',  0xe1b6 => 'icon-hipster-location_disabled',  0xe1b8 => 'icon-hipster-graphic_eq',  0xe024 => 'icon-hipster-high_quality',
					0xe02f => 'icon-hipster-library_books',  0xe1b9 => 'icon-hipster-network_cell',  0xe059 => 'icon-hipster-replay_10',  0xe05a => 'icon-hipster-replay_30',  0xe05b => 'icon-hipster-replay_5',
				),
				'tags' => array(
					'folder', 'file', 'pdf', 'libre', 'excel', 'word', 'powerpoint', 'html', 'xml', 'csv', 'zip', 'drawer', 'drawers', 'cabinet',
				),
			),
			array(
				'id' => 'travel',
				'label' => 'Travel and Living',
				'elements' => array(
					0xe544 => 'Local drink',  0xe034 => 'Pause',
				),
				'element_classes' => array(
					0xe544 => 'icon-hipster-local_drink',  0xe034 => 'icon-hipster-pause',
				),
				'tags' => array(
					'coffee', 'knife', 'fork', 'road', 'plane', 'jet', 'truck', 'raod', 'ticket', 'cinema', 'prize', 'drink', 'beverage',
				),
			),
			array(
				'id' => 'weather',
				'label' => 'Weather & Nature Icons',
				'elements' => array(
					0xe0cb => 'Chat bubble outline',  0xe42d => 'Wb cloudy',  0xe2be => 'Cloud circle',  0xe2bf => 'Cloud done',  0xe2c1 => 'Cloud off',  0xe2c2 => 'Cloud queue',
					0xe0ba => 'Contacts',
				),
				'element_classes' => array(
					0xe0cb => 'icon-hipster-chat_bubble_outline',  0xe42d => 'icon-hipster-wb_cloudy',  0xe2be => 'icon-hipster-cloud_circle',  0xe2bf => 'icon-hipster-cloud_done',  0xe2c1 => 'icon-hipster-cloud_off',  0xe2c2 => 'icon-hipster-cloud_queue',
					0xe0ba => 'icon-hipster-contacts',
				),
				'tags' => array(
					'leaf', 'sun', 'sunrise', 'windy', 'snow', 'snowflake', 'cloudy', 'cloud', 'weather', 'moon', 'wind', 'rain', 'rainy', 'lightning', 'snowy', 'fire',
				),
			),
			array(
				'id' => 'like_dislike',
				'label' => 'Like & Dislike Icons',
				'elements' => array(
					0xe0d0 => 'Contact mail',  0xe0cf => 'Contact phone',  0xe838 => 'Star',  0xe0c6 => 'Live help',  0xe0ce => 'Portable wifi off',  0xe0d1 => 'Ring volume',
					0xe0d2 => 'Speaker phone',  0xe83a => 'Star border',  0xe839 => 'Star half',  0xe0d5 => 'Stay primary landscape',  0xe8db => 'Thumb down',  0xe8dc => 'Thumb up',
					0xe8dd => 'Thumbs up down',
				),
				'element_classes' => array(
					0xe0d0 => 'icon-hipster-contact_mail',  0xe0cf => 'icon-hipster-contact_phone',  0xe838 => 'icon-hipster-star',  0xe0c6 => 'icon-hipster-live_help',  0xe0ce => 'icon-hipster-portable_wifi_off',  0xe0d1 => 'icon-hipster-ring_volume',
					0xe0d2 => 'icon-hipster-speaker_phone',  0xe83a => 'icon-hipster-star_border',  0xe839 => 'icon-hipster-star_half',  0xe0d5 => 'icon-hipster-stay_primary_landscape',  0xe8db => 'icon-hipster-thumb_down',  0xe8dc => 'icon-hipster-thumb_up',
					0xe8dd => 'icon-hipster-thumbs_up_down',
				),
				'tags' => array(
					'thumb', 'thumbs', 'heart', 'star', 'eye',
				),
			),
			array(
				'id' => 'emoticons',
				'label' => 'Emoticons',
				'elements' => array(
					0xe0e4 => 'Call missed outgoing',  0xe0e0 => 'Import contacts',  0xe0e1 => 'Mail outline',  0xe0db => 'Phonelink erase',  0xe0dc => 'Phonelink lock',  0xe0dd => 'Phonelink ring',
					0xe0de => 'Phonelink setup',  0xe0df => 'Present to all',  0xe0e5 => 'Rss feed',  0xe0e2 => 'Screen share',  0xe812 => 'Sentiment neutral',  0xe0d6 => 'Stay primary portrait',
					0xe0d8 => 'Textsms',  0xe0e3 => 'Stop screen share',  0xe0d7 => 'Swap calls',  0xe0d9 => 'Voicemail',  0xe0da => 'Vpn key',
				),
				'element_classes' => array(
					0xe0e4 => 'icon-hipster-call_missed_outgoing',  0xe0e0 => 'icon-hipster-import_contacts',  0xe0e1 => 'icon-hipster-mail_outline',  0xe0db => 'icon-hipster-phonelink_erase',  0xe0dc => 'icon-hipster-phonelink_lock',  0xe0dd => 'icon-hipster-phonelink_ring',
					0xe0de => 'icon-hipster-phonelink_setup',  0xe0df => 'icon-hipster-present_to_all',  0xe0e5 => 'icon-hipster-rss_feed',  0xe0e2 => 'icon-hipster-screen_share',  0xe812 => 'icon-hipster-sentiment_neutral',  0xe0d6 => 'icon-hipster-stay_primary_portrait',
					0xe0d8 => 'icon-hipster-textsms',  0xe0e3 => 'icon-hipster-stop_screen_share',  0xe0d7 => 'icon-hipster-swap_calls',  0xe0d9 => 'icon-hipster-voicemail',  0xe0da => 'icon-hipster-vpn_key',
				),
				'tags' => array(
					'smile', 'smiley', 'emot', 'emoticon', 'emoticons', 'meh', 'frown', 'happy', 'sad', 'angry', 'cool', 'wink', 'grin', 'evil', 'shocked', 'tongue', 'tease', 'teaser', 'confused', 'neutral', 'wondering',
				),
			),
			array(
				'id' => 'arrow',
				'label' => 'Directional Icons',
				'elements' => array(
					0xe3ba => 'Control point',  0xe5c4 => 'Arrow back',  0xe5db => 'Arrow downward',  0xe5c5 => 'Arrow drop down',  0xe5c6 => 'Arrow drop down circle',  0xe5c7 => 'Arrow drop up',
					0xe5d8 => 'Arrow upward',  0xe3bb => 'Control point duplicate',  0xe044 => 'Skip next',  0xe5d9 => 'Subdirectory arrow left',  0xe5da => 'Subdirectory arrow right',  0xe069 => 'Web asset',

				),
				'element_classes' => array(
					0xe3ba => 'icon-hipster-control_point',  0xe5c4 => 'icon-hipster-arrow_back',  0xe5db => 'icon-hipster-arrow_downward',  0xe5c5 => 'icon-hipster-arrow_drop_down',  0xe5c6 => 'icon-hipster-arrow_drop_down_circle',  0xe5c7 => 'icon-hipster-arrow_drop_up',
					0xe5d8 => 'icon-hipster-arrow_upward',  0xe3bb => 'icon-hipster-control_point_duplicate',  0xe044 => 'icon-hipster-skip_next',  0xe5d9 => 'icon-hipster-subdirectory_arrow_left',  0xe5da => 'icon-hipster-subdirectory_arrow_right',  0xe069 => 'icon-hipster-web_asset',

				),
				'tags' => array(
					'arrow', 'point', 'direction', 'directional',
				),
			),
			array(
				'id' => 'others',
				'label' => 'Other Icons',
				'elements' => array(
					0xe84d => ' 3',  0xeb3b => 'Ac unit',  0xe855 => 'Alarm',  0xe8b5 => 'Schedule',  0xe84e => 'Accessibility',  0xe914 => 'Accessible',
					0xe84f => 'Account balance',  0xe850 => 'Account balance wallet',  0xe853 => 'Account circle',  0xe60e => 'Adb',  0xe856 => 'Alarm add',  0xe39e => 'Adjust',
					0xe630 => 'Airline seat flat',  0xe631 => 'Airline seat flat angled',  0xe632 => 'Airline seat individual suite',  0xe633 => 'Airline seat legroom extra',  0xe634 => 'Airline seat legroom normal',  0xe635 => 'Airline seat legroom reduced',
					0xe636 => 'Airline seat recline extra',  0xe637 => 'Airline seat recline normal',  0xe539 => 'Flight',  0xeb3c => 'Airport shuttle',  0xe857 => 'Alarm off',  0xe858 => 'Alarm on',
					0xe019 => 'Album',  0xeb3d => 'All inclusive',  0xe90b => 'All out',  0xe85a => 'Announcement',  0xe5c3 => 'Apps',  0xe85b => 'Aspect ratio',
					0xe801 => 'Poll',  0xe85d => 'Assignment',  0xe85e => 'Assignment ind',  0xe85f => 'Assignment late',  0xe860 => 'Assignment return',  0xe861 => 'Assignment returned',
					0xe862 => 'Assignment turned in',  0xe39f => 'Assistant',  0xe3a1 => 'Audiotrack',  0xe863 => 'Autorenew',  0xe01b => 'Av timer',  0xeb3e => 'Beach access',
					0xe52d => 'Beenhere',  0xe3a2 => 'Blur circular',  0xe3a3 => 'Blur linear',  0xe3a4 => 'Blur off',  0xe3a5 => 'Blur on',  0xe86e => 'Class',
					0xe8e6 => 'Turned in',  0xe8e7 => 'Turned in not',  0xe228 => 'Border all',  0xe229 => 'Border bottom',  0xe22a => 'Border clear',  0xe22b => 'Border color',
					0xe22c => 'Border horizontal',  0xe22d => 'Border inner',  0xe22e => 'Border left',  0xe22f => 'Border outer',  0xe230 => 'Border right',  0xe231 => 'Border style',
					0xe232 => 'Border top',  0xe233 => 'Border vertical',  0xe868 => 'Bug report',  0xe869 => 'Build',  0xe43c => 'Burst mode',  0xe7ee => 'Domain',
					0xeb3f => 'Business center',  0xe86a => 'Cached',  0xe7e9 => 'Cake',  0xe0b1 => 'Call end',  0xe0b2 => 'Call made',  0xe252 => 'Merge type',
					0xe3af => 'Camera',  0xe8fc => 'Camera enhance',  0xe3b1 => 'Camera front',  0xe3b2 => 'Camera rear',  0xe3b3 => 'Camera roll',  0xe8b1 => 'Redeem',
					0xeb40 => 'Casino',  0xe307 => 'Cast',  0xe308 => 'Cast connected',  0xe3b4 => 'Center focus strong',  0xe3b5 => 'Center focus weak',  0xe408 => 'Navigate before',
					0xeb41 => 'Child care',  0xeb42 => 'Child friendly',  0xe5cd => 'Close',  0xe40a => 'Palette',  0xe3b8 => 'Colorize',  0xe0b9 => 'Comment',
					0xe3b9 => 'Compare',  0xe915 => 'Compare arrows',  0xe31e => 'Laptop',  0xe638 => 'Confirmation number',  0xe90c => 'Copyright',  0xe254 => 'Mode edit',
					0xe8a1 => 'Payment',  0xe3be => 'Crop',  0xe3bc => 'Crop  16',  0xe3bd => 'Crop  3',  0xe3c3 => 'Crop landscape',  0xe3c0 => 'Crop  7',
					0xe3c1 => 'Crop din',  0xe3c2 => 'Crop free',  0xe3c4 => 'Crop original',  0xe3c5 => 'Crop portrait',  0xe3c6 => 'Crop square',  0xe871 => 'Dashboard',
					0xe916 => 'Date range',  0xe3c7 => 'Dehaze',  0xe872 => 'Delete',  0xe92b => 'Delete forever',  0xe873 => 'Description',  0xe3c8 => 'Details',
					0xe30d => 'Developer board',  0xe335 => 'Device hub',  0xe326 => 'Phonelink',  0xe337 => 'Devices other',  0xe52e => 'Directions',  0xe52f => 'Directions bike',
					0xe532 => 'Directions boat',  0xe530 => 'Directions bus',  0xe531 => 'Directions car',  0xe534 => 'Directions railway',  0xe566 => 'Directions run',  0xe535 => 'Directions transit',
					0xe536 => 'Directions walk',  0xe610 => 'Disc full',  0xe875 => 'Dns',  0xe611 => 'Do not disturb alt',  0xe643 => 'Do not disturb off',  0xe30e => 'Dock',
					0xe876 => 'Done',  0xe877 => 'Done all',  0xe917 => 'Donut large',  0xe918 => 'Donut small',  0xe25d => 'Drag handle',  0xe62c => 'Time to leave',
					0xe8fb => 'Eject',  0xe63f => 'Enhanced encryption',  0xe56d => 'Ev station',  0xe24f => 'Insert invitation',  0xe614 => 'Event available',  0xe616 => 'Event note',
					0xe903 => 'Event seat',  0xe87a => 'Explore',  0xe3ca => 'Exposure',  0xe3cb => 'Exposure neg  1',  0xe3cc => 'Exposure neg  2',  0xe3cd => 'Exposure plus  1',
					0xe3ce => 'Exposure plus  2',  0xe3cf => 'Exposure zero',  0xe87b => 'Extension',  0xe87c => 'Face',  0xe87d => 'Favorite',  0xe87e => 'Favorite border',
					0xe626 => 'Sms failed',  0xe884 => 'Get app',  0xe564 => 'Terrain',  0xe880 => 'Find in page',  0xe881 => 'Find replace',  0xe90d => 'Fingerprint',
					0xe5dc => 'First page',  0xeb43 => 'Fitness center',  0xe3e4 => 'Flare',  0xe3e5 => 'Flash auto',  0xe3e6 => 'Flash off',  0xe3e7 => 'Flash on',
					0xe904 => 'Flight land',  0xe905 => 'Flight takeoff',  0xe239 => 'Format clear',  0xe23a => 'Format color fill',  0xe23b => 'Format color reset',  0xe23c => 'Format color text',
					0xe25e => 'Format shapes',  0xe245 => 'Format size',  0xe247 => 'Format textdirection l to r',  0xe248 => 'Format textdirection r to l',  0xe249 => 'Format underlined',  0xeb44 => 'Free breakfast',
					0xe5d0 => 'Fullscreen',  0xe24a => 'Functions',  0xe927 => 'G translate',  0xe90e => 'Gavel',  0xe908 => 'Gif',  0xe900 => 'Goat',
					0xeb45 => 'Golf course',  0xe3e9 => 'Gradient',  0xe3ea => 'Grain',  0xe3eb => 'Grid off',  0xe3ec => 'Grid on',  0xe7fb => 'People',
					0xe3ed => 'Hdr off',  0xe3ee => 'Hdr on',  0xe3f1 => 'Hdr strong',  0xe3f2 => 'Hdr weak',  0xe310 => 'Headset',  0xe311 => 'Headset mic',
					0xe3f3 => 'Healing',  0xe25f => 'Highlight',  0xe888 => 'Highlight off',  0xe8b3 => 'Restore',  0xeb46 => 'Hot tub',  0xe549 => 'Local hotel',
					0xe88b => 'Hourglass empty',  0xe88c => 'Hourglass full',  0xe902 => 'Http',  0xe912 => 'Important devices',  0xe890 => 'Input',  0xe891 => 'Invert colors',
					0xe3f6 => 'Iso',  0xeb47 => 'Kitchen',  0xe892 => 'Label',  0xe893 => 'Label outline',  0xe894 => 'Language',  0xe31f => 'Laptop chromebook',
					0xe320 => 'Laptop mac',  0xe5dd => 'Last page',  0xe89e => 'Open in new',  0xe53b => 'Layers',  0xe53c => 'Layers clear',  0xe3f8 => 'Leak add',
					0xe3fa => 'Lens',  0xe90f => 'Lightbulb outline',  0xe260 => 'Linear scale',  0xe438 => 'Linked camera',  0xe639 => 'Live tv',  0xe53d => 'Local airport',
					0xe53e => 'Local atm',  0xe540 => 'Local bar',  0xe541 => 'Local cafe',  0xe542 => 'Local car wash',  0xe543 => 'Local convenience store',  0xe545 => 'Local florist',
					0xe546 => 'Local gas station',  0xe548 => 'Local hospital',  0xe54a => 'Local laundry service',  0xe54c => 'Local mall',  0xe8da => 'Theaters',  0xe54e => 'Local offer',
					0xe54f => 'Local parking',  0xe550 => 'Local pharmacy',  0xe552 => 'Local pizza',  0xe8ad => 'Print',  0xe558 => 'Local shipping',  0xe559 => 'Local taxi',
					0xe8b4 => 'Room',  0xe3fc => 'Looks',  0xe3fb => 'Looks  3',  0xe3fd => 'Looks  4',  0xe3fe => 'Looks  5',  0xe3ff => 'Looks  6',
					0xe400 => 'Looks one',  0xe401 => 'Looks two',  0xe627 => 'Sync',  0xe402 => 'Loupe',  0xe89a => 'Loyalty',  0xe89b => 'Markunread mailbox',
					0xe322 => 'Memory',  0xe618 => 'Mms',  0xe263 => 'Monetization on',  0xe7f3 => 'Mood bad',  0xe619 => 'More',  0xe5d3 => 'More horiz',
					0xe5d4 => 'More vert',  0xe91b => 'Motorcycle',  0xe404 => 'Movie creation',  0xe406 => 'Nature',  0xe407 => 'Nature people',  0xe55d => 'Navigation',
					0xe569 => 'Near me',  0xe61a => 'Network locked',  0xe641 => 'No encryption',  0xe1ce => 'Signal cellular no sim',  0xe89c => 'Note add',  0xe7f4 => 'Notifications',
					0xe7f7 => 'Notifications active',  0xe7f5 => 'Notifications none',  0xe7f6 => 'Notifications off',  0xe7f8 => 'Notifications paused',  0xe91c => 'Opacity',  0xe89f => 'Open with',
					0xe7f9 => 'Pages',  0xe8a0 => 'Pageview',  0xe40b => 'Panorama',  0xe40d => 'Panorama horizontal',  0xe40e => 'Panorama vertical',  0xe40f => 'Panorama wide angle',
					0xe7fa => 'Party mode',  0xe7fc => 'People outline',  0xe8a2 => 'Perm camera mic',  0xe8a4 => 'Perm data setting',  0xe8a5 => 'Perm device information',  0xe7ff => 'Person outline',
					0xe8a9 => 'Perm scan wifi',  0xe7fd => 'Person',  0xe7fe => 'Person add',  0xe91d => 'Pets',  0xe327 => 'Phonelink off',  0xe800 => 'Plus one',
					0xe8ab => 'Polymer',  0xeb48 => 'Pool',  0xe416 => 'Portrait',  0xe63c => 'Power',  0xe336 => 'Power input',  0xe91e => 'Pregnant woman',
					0xe645 => 'Priority high',  0xe80b => 'Public',  0xe255 => 'Publish',  0xe560 => 'Rate review',  0xe8b0 => 'Receipt',  0xe91f => 'Record voice over',
					0xe5d5 => 'Refresh',  0xe15d => 'Remove circle outline',  0xe8f4 => 'Visibility',  0xe8fe => 'Reorder',  0xe56c => 'Restaurant',  0xe929 => 'Restore page',
					0xeb49 => 'Room service',  0xe920 => 'Rounded corner',  0xe328 => 'Router',  0xe921 => 'Rowing',  0xe642 => 'Rv hookup',  0xe562 => 'Satellite',
					0xe329 => 'Scanner',  0xe80c => 'School',  0xe1c2 => 'Sd storage',  0xe32a => 'Security',  0xe163 => 'Send',  0xe811 => 'Sentiment dissatisfied',
					0xe813 => 'Sentiment satisfied',  0xe814 => 'Sentiment very dissatisfied',  0xe815 => 'Sentiment very satisfied',  0xe8c9 => 'Shop',  0xe8ca => 'Shop two',  0xe8cb => 'Shopping basket',
					0xe261 => 'Short text',  0xe1c8 => 'Signal cellular  4',  0xe1cd => 'Signal cellular connected no internet  4',  0xe1cf => 'Signal cellular null',  0xe1d0 => 'Signal cellular off',  0xe1d8 => 'Signal wifi  4',
					0xe1d9 => 'Signal wifi  4',  0xe1da => 'Signal wifi off',  0xe41b => 'Slideshow',  0xeb4a => 'Smoke free',  0xeb4b => 'Smoking rooms',  0xeb4c => 'Spa',
					0xe256 => 'Space bar',  0xe32d => 'Speaker',  0xe8cd => 'Speaker notes',  0xe92a => 'Speaker notes off',  0xe8ce => 'Spellcheck',  0xe8d0 => 'Stars',
					0xe1db => 'Storage',  0xe563 => 'Store mall directory',  0xe41c => 'Straighten',  0xe56e => 'Streetview',  0xe41d => 'Style',  0xe8d2 => 'Subject',
					0xe56f => 'Subway',  0xe8d3 => 'Supervisor account',  0xe8d4 => 'Swap horiz',  0xe8d5 => 'Swap vert',  0xe8d6 => 'Swap vertical circle',  0xe41e => 'Switch camera',
					0xe628 => 'Sync disabled',  0xe629 => 'Sync problem',  0xe62a => 'System update',  0xe8d7 => 'System update alt',  0xe8d8 => 'Tab',  0xe8d9 => 'Tab unselected',
					0xe262 => 'Text fields',  0xe421 => 'Texture',  0xe422 => 'Timelapse',  0xe922 => 'Timeline',  0xe425 => 'Timer',  0xe423 => 'Timer  10',
					0xe424 => 'Timer  3',  0xe426 => 'Timer off',  0xe264 => 'Title',  0xe8de => 'Toc',  0xe8df => 'Today',  0xe8e0 => 'Toll',
					0xe427 => 'Tonality',  0xe913 => 'Touch app',  0xe332 => 'Toys',  0xe8e1 => 'Track changes',  0xe565 => 'Traffic',  0xe570 => 'Train',
					0xe571 => 'Tram',  0xe572 => 'Transfer within a station',  0xe428 => 'Transform',  0xe8e2 => 'Translate',  0xe8e3 => 'Trending down',  0xe8e4 => 'Trending flat',
					0xe8e5 => 'Trending up',  0xe429 => 'Tune',  0xe333 => 'Tv',  0xe5d6 => 'Unfold less',  0xe5d7 => 'Unfold more',  0xe923 => 'Update',
					0xe1e0 => 'Usb',  0xe62d => 'Vibration',  0xe04c => 'Videocam off',  0xe338 => 'Videogame asset',  0xe8e9 => 'View agenda',  0xe8ea => 'View array',
					0xe8eb => 'View carousel',  0xe42a => 'View comfy',  0xe42b => 'View compact',  0xe8ed => 'View day',  0xe8ee => 'View headline',  0xe8f0 => 'View module',
					0xe8f1 => 'View quilt',  0xe8f2 => 'View stream',  0xe8f3 => 'View week',  0xe435 => 'Vignette',  0xe8f5 => 'Visibility off',  0xe62e => 'Voice chat',
					0xe42c => 'Wb auto',  0xe42e => 'Wb incandescent',  0xe436 => 'Wb iridescent',  0xe430 => 'Wb sunny',  0xe63d => 'Wc',  0xe80e => 'Whatshot',
					0xe63e => 'Wifi',  0xe1e2 => 'Wifi tethering',  0xe25b => 'Wrap text',  0xe8fa => 'Youtube searched for',  0xe8ff => 'Zoom in',  0xe901 => 'Zoom out',

				),
				'element_classes' => array(
					0xe84d => 'icon-hipster-3d_rotation',  0xeb3b => 'icon-hipster-ac_unit',  0xe855 => 'icon-hipster-alarm',  0xe8b5 => 'icon-hipster-schedule',  0xe84e => 'icon-hipster-accessibility',  0xe914 => 'icon-hipster-accessible',
					0xe84f => 'icon-hipster-account_balance',  0xe850 => 'icon-hipster-account_balance_wallet',  0xe853 => 'icon-hipster-account_circle',  0xe60e => 'icon-hipster-adb',  0xe856 => 'icon-hipster-alarm_add',  0xe39e => 'icon-hipster-adjust',
					0xe630 => 'icon-hipster-airline_seat_flat',  0xe631 => 'icon-hipster-airline_seat_flat_angled',  0xe632 => 'icon-hipster-airline_seat_individual_suite',  0xe633 => 'icon-hipster-airline_seat_legroom_extra',  0xe634 => 'icon-hipster-airline_seat_legroom_normal',  0xe635 => 'icon-hipster-airline_seat_legroom_reduced',
					0xe636 => 'icon-hipster-airline_seat_recline_extra',  0xe637 => 'icon-hipster-airline_seat_recline_normal',  0xe539 => 'icon-hipster-flight',  0xeb3c => 'icon-hipster-airport_shuttle',  0xe857 => 'icon-hipster-alarm_off',  0xe858 => 'icon-hipster-alarm_on',
					0xe019 => 'icon-hipster-album',  0xeb3d => 'icon-hipster-all_inclusive',  0xe90b => 'icon-hipster-all_out',  0xe85a => 'icon-hipster-announcement',  0xe5c3 => 'icon-hipster-apps',  0xe85b => 'icon-hipster-aspect_ratio',
					0xe801 => 'icon-hipster-poll',  0xe85d => 'icon-hipster-assignment',  0xe85e => 'icon-hipster-assignment_ind',  0xe85f => 'icon-hipster-assignment_late',  0xe860 => 'icon-hipster-assignment_return',  0xe861 => 'icon-hipster-assignment_returned',
					0xe862 => 'icon-hipster-assignment_turned_in',  0xe39f => 'icon-hipster-assistant',  0xe3a1 => 'icon-hipster-audiotrack',  0xe863 => 'icon-hipster-autorenew',  0xe01b => 'icon-hipster-av_timer',  0xeb3e => 'icon-hipster-beach_access',
					0xe52d => 'icon-hipster-beenhere',  0xe3a2 => 'icon-hipster-blur_circular',  0xe3a3 => 'icon-hipster-blur_linear',  0xe3a4 => 'icon-hipster-blur_off',  0xe3a5 => 'icon-hipster-blur_on',  0xe86e => 'icon-hipster-class',
					0xe8e6 => 'icon-hipster-turned_in',  0xe8e7 => 'icon-hipster-turned_in_not',  0xe228 => 'icon-hipster-border_all',  0xe229 => 'icon-hipster-border_bottom',  0xe22a => 'icon-hipster-border_clear',  0xe22b => 'icon-hipster-border_color',
					0xe22c => 'icon-hipster-border_horizontal',  0xe22d => 'icon-hipster-border_inner',  0xe22e => 'icon-hipster-border_left',  0xe22f => 'icon-hipster-border_outer',  0xe230 => 'icon-hipster-border_right',  0xe231 => 'icon-hipster-border_style',
					0xe232 => 'icon-hipster-border_top',  0xe233 => 'icon-hipster-border_vertical',  0xe868 => 'icon-hipster-bug_report',  0xe869 => 'icon-hipster-build',  0xe43c => 'icon-hipster-burst_mode',  0xe7ee => 'icon-hipster-domain',
					0xeb3f => 'icon-hipster-business_center',  0xe86a => 'icon-hipster-cached',  0xe7e9 => 'icon-hipster-cake',  0xe0b1 => 'icon-hipster-call_end',  0xe0b2 => 'icon-hipster-call_made',  0xe252 => 'icon-hipster-merge_type',
					0xe3af => 'icon-hipster-camera',  0xe8fc => 'icon-hipster-camera_enhance',  0xe3b1 => 'icon-hipster-camera_front',  0xe3b2 => 'icon-hipster-camera_rear',  0xe3b3 => 'icon-hipster-camera_roll',  0xe8b1 => 'icon-hipster-redeem',
					0xeb40 => 'icon-hipster-casino',  0xe307 => 'icon-hipster-cast',  0xe308 => 'icon-hipster-cast_connected',  0xe3b4 => 'icon-hipster-center_focus_strong',  0xe3b5 => 'icon-hipster-center_focus_weak',  0xe408 => 'icon-hipster-navigate_before',
					0xeb41 => 'icon-hipster-child_care',  0xeb42 => 'icon-hipster-child_friendly',  0xe5cd => 'icon-hipster-close',  0xe40a => 'icon-hipster-palette',  0xe3b8 => 'icon-hipster-colorize',  0xe0b9 => 'icon-hipster-comment',
					0xe3b9 => 'icon-hipster-compare',  0xe915 => 'icon-hipster-compare_arrows',  0xe31e => 'icon-hipster-laptop',  0xe638 => 'icon-hipster-confirmation_number',  0xe90c => 'icon-hipster-copyright',  0xe254 => 'icon-hipster-mode_edit',
					0xe8a1 => 'icon-hipster-payment',  0xe3be => 'icon-hipster-crop',  0xe3bc => 'icon-hipster-crop_16_9',  0xe3bd => 'icon-hipster-crop_3_2',  0xe3c3 => 'icon-hipster-crop_landscape',  0xe3c0 => 'icon-hipster-crop_7_5',
					0xe3c1 => 'icon-hipster-crop_din',  0xe3c2 => 'icon-hipster-crop_free',  0xe3c4 => 'icon-hipster-crop_original',  0xe3c5 => 'icon-hipster-crop_portrait',  0xe3c6 => 'icon-hipster-crop_square',  0xe871 => 'icon-hipster-dashboard',
					0xe916 => 'icon-hipster-date_range',  0xe3c7 => 'icon-hipster-dehaze',  0xe872 => 'icon-hipster-delete',  0xe92b => 'icon-hipster-delete_forever',  0xe873 => 'icon-hipster-description',  0xe3c8 => 'icon-hipster-details',
					0xe30d => 'icon-hipster-developer_board',  0xe335 => 'icon-hipster-device_hub',  0xe326 => 'icon-hipster-phonelink',  0xe337 => 'icon-hipster-devices_other',  0xe52e => 'icon-hipster-directions',  0xe52f => 'icon-hipster-directions_bike',
					0xe532 => 'icon-hipster-directions_boat',  0xe530 => 'icon-hipster-directions_bus',  0xe531 => 'icon-hipster-directions_car',  0xe534 => 'icon-hipster-directions_railway',  0xe566 => 'icon-hipster-directions_run',  0xe535 => 'icon-hipster-directions_transit',
					0xe536 => 'icon-hipster-directions_walk',  0xe610 => 'icon-hipster-disc_full',  0xe875 => 'icon-hipster-dns',  0xe611 => 'icon-hipster-do_not_disturb_alt',  0xe643 => 'icon-hipster-do_not_disturb_off',  0xe30e => 'icon-hipster-dock',
					0xe876 => 'icon-hipster-done',  0xe877 => 'icon-hipster-done_all',  0xe917 => 'icon-hipster-donut_large',  0xe918 => 'icon-hipster-donut_small',  0xe25d => 'icon-hipster-drag_handle',  0xe62c => 'icon-hipster-time_to_leave',
					0xe8fb => 'icon-hipster-eject',  0xe63f => 'icon-hipster-enhanced_encryption',  0xe56d => 'icon-hipster-ev_station',  0xe24f => 'icon-hipster-insert_invitation',  0xe614 => 'icon-hipster-event_available',  0xe616 => 'icon-hipster-event_note',
					0xe903 => 'icon-hipster-event_seat',  0xe87a => 'icon-hipster-explore',  0xe3ca => 'icon-hipster-exposure',  0xe3cb => 'icon-hipster-exposure_neg_1',  0xe3cc => 'icon-hipster-exposure_neg_2',  0xe3cd => 'icon-hipster-exposure_plus_1',
					0xe3ce => 'icon-hipster-exposure_plus_2',  0xe3cf => 'icon-hipster-exposure_zero',  0xe87b => 'icon-hipster-extension',  0xe87c => 'icon-hipster-face',  0xe87d => 'icon-hipster-favorite',  0xe87e => 'icon-hipster-favorite_border',
					0xe626 => 'icon-hipster-sms_failed',  0xe884 => 'icon-hipster-get_app',  0xe564 => 'icon-hipster-terrain',  0xe880 => 'icon-hipster-find_in_page',  0xe881 => 'icon-hipster-find_replace',  0xe90d => 'icon-hipster-fingerprint',
					0xe5dc => 'icon-hipster-first_page',  0xeb43 => 'icon-hipster-fitness_center',  0xe3e4 => 'icon-hipster-flare',  0xe3e5 => 'icon-hipster-flash_auto',  0xe3e6 => 'icon-hipster-flash_off',  0xe3e7 => 'icon-hipster-flash_on',
					0xe904 => 'icon-hipster-flight_land',  0xe905 => 'icon-hipster-flight_takeoff',  0xe239 => 'icon-hipster-format_clear',  0xe23a => 'icon-hipster-format_color_fill',  0xe23b => 'icon-hipster-format_color_reset',  0xe23c => 'icon-hipster-format_color_text',
					0xe25e => 'icon-hipster-format_shapes',  0xe245 => 'icon-hipster-format_size',  0xe247 => 'icon-hipster-format_textdirection_l_to_r',  0xe248 => 'icon-hipster-format_textdirection_r_to_l',  0xe249 => 'icon-hipster-format_underlined',  0xeb44 => 'icon-hipster-free_breakfast',
					0xe5d0 => 'icon-hipster-fullscreen',  0xe24a => 'icon-hipster-functions',  0xe927 => 'icon-hipster-g_translate',  0xe90e => 'icon-hipster-gavel',  0xe908 => 'icon-hipster-gif',  0xe900 => 'icon-hipster-goat',
					0xeb45 => 'icon-hipster-golf_course',  0xe3e9 => 'icon-hipster-gradient',  0xe3ea => 'icon-hipster-grain',  0xe3eb => 'icon-hipster-grid_off',  0xe3ec => 'icon-hipster-grid_on',  0xe7fb => 'icon-hipster-people',
					0xe3ed => 'icon-hipster-hdr_off',  0xe3ee => 'icon-hipster-hdr_on',  0xe3f1 => 'icon-hipster-hdr_strong',  0xe3f2 => 'icon-hipster-hdr_weak',  0xe310 => 'icon-hipster-headset',  0xe311 => 'icon-hipster-headset_mic',
					0xe3f3 => 'icon-hipster-healing',  0xe25f => 'icon-hipster-highlight',  0xe888 => 'icon-hipster-highlight_off',  0xe8b3 => 'icon-hipster-restore',  0xeb46 => 'icon-hipster-hot_tub',  0xe549 => 'icon-hipster-local_hotel',
					0xe88b => 'icon-hipster-hourglass_empty',  0xe88c => 'icon-hipster-hourglass_full',  0xe902 => 'icon-hipster-http',  0xe912 => 'icon-hipster-important_devices',  0xe890 => 'icon-hipster-input',  0xe891 => 'icon-hipster-invert_colors',
					0xe3f6 => 'icon-hipster-iso',  0xeb47 => 'icon-hipster-kitchen',  0xe892 => 'icon-hipster-label',  0xe893 => 'icon-hipster-label_outline',  0xe894 => 'icon-hipster-language',  0xe31f => 'icon-hipster-laptop_chromebook',
					0xe320 => 'icon-hipster-laptop_mac',  0xe5dd => 'icon-hipster-last_page',  0xe89e => 'icon-hipster-open_in_new',  0xe53b => 'icon-hipster-layers',  0xe53c => 'icon-hipster-layers_clear',  0xe3f8 => 'icon-hipster-leak_add',
					0xe3fa => 'icon-hipster-lens',  0xe90f => 'icon-hipster-lightbulb_outline',  0xe260 => 'icon-hipster-linear_scale',  0xe438 => 'icon-hipster-linked_camera',  0xe639 => 'icon-hipster-live_tv',  0xe53d => 'icon-hipster-local_airport',
					0xe53e => 'icon-hipster-local_atm',  0xe540 => 'icon-hipster-local_bar',  0xe541 => 'icon-hipster-local_cafe',  0xe542 => 'icon-hipster-local_car_wash',  0xe543 => 'icon-hipster-local_convenience_store',  0xe545 => 'icon-hipster-local_florist',
					0xe546 => 'icon-hipster-local_gas_station',  0xe548 => 'icon-hipster-local_hospital',  0xe54a => 'icon-hipster-local_laundry_service',  0xe54c => 'icon-hipster-local_mall',  0xe8da => 'icon-hipster-theaters',  0xe54e => 'icon-hipster-local_offer',
					0xe54f => 'icon-hipster-local_parking',  0xe550 => 'icon-hipster-local_pharmacy',  0xe552 => 'icon-hipster-local_pizza',  0xe8ad => 'icon-hipster-print',  0xe558 => 'icon-hipster-local_shipping',  0xe559 => 'icon-hipster-local_taxi',
					0xe8b4 => 'icon-hipster-room',  0xe3fc => 'icon-hipster-looks',  0xe3fb => 'icon-hipster-looks_3',  0xe3fd => 'icon-hipster-looks_4',  0xe3fe => 'icon-hipster-looks_5',  0xe3ff => 'icon-hipster-looks_6',
					0xe400 => 'icon-hipster-looks_one',  0xe401 => 'icon-hipster-looks_two',  0xe627 => 'icon-hipster-sync',  0xe402 => 'icon-hipster-loupe',  0xe89a => 'icon-hipster-loyalty',  0xe89b => 'icon-hipster-markunread_mailbox',
					0xe322 => 'icon-hipster-memory',  0xe618 => 'icon-hipster-mms',  0xe263 => 'icon-hipster-monetization_on',  0xe7f3 => 'icon-hipster-mood_bad',  0xe619 => 'icon-hipster-more',  0xe5d3 => 'icon-hipster-more_horiz',
					0xe5d4 => 'icon-hipster-more_vert',  0xe91b => 'icon-hipster-motorcycle',  0xe404 => 'icon-hipster-movie_creation',  0xe406 => 'icon-hipster-nature',  0xe407 => 'icon-hipster-nature_people',  0xe55d => 'icon-hipster-navigation',
					0xe569 => 'icon-hipster-near_me',  0xe61a => 'icon-hipster-network_locked',  0xe641 => 'icon-hipster-no_encryption',  0xe1ce => 'icon-hipster-signal_cellular_no_sim',  0xe89c => 'icon-hipster-note_add',  0xe7f4 => 'icon-hipster-notifications',
					0xe7f7 => 'icon-hipster-notifications_active',  0xe7f5 => 'icon-hipster-notifications_none',  0xe7f6 => 'icon-hipster-notifications_off',  0xe7f8 => 'icon-hipster-notifications_paused',  0xe91c => 'icon-hipster-opacity',  0xe89f => 'icon-hipster-open_with',
					0xe7f9 => 'icon-hipster-pages',  0xe8a0 => 'icon-hipster-pageview',  0xe40b => 'icon-hipster-panorama',  0xe40d => 'icon-hipster-panorama_horizontal',  0xe40e => 'icon-hipster-panorama_vertical',  0xe40f => 'icon-hipster-panorama_wide_angle',
					0xe7fa => 'icon-hipster-party_mode',  0xe7fc => 'icon-hipster-people_outline',  0xe8a2 => 'icon-hipster-perm_camera_mic',  0xe8a4 => 'icon-hipster-perm_data_setting',  0xe8a5 => 'icon-hipster-perm_device_information',  0xe7ff => 'icon-hipster-person_outline',
					0xe8a9 => 'icon-hipster-perm_scan_wifi',  0xe7fd => 'icon-hipster-person',  0xe7fe => 'icon-hipster-person_add',  0xe91d => 'icon-hipster-pets',  0xe327 => 'icon-hipster-phonelink_off',  0xe800 => 'icon-hipster-plus_one',
					0xe8ab => 'icon-hipster-polymer',  0xeb48 => 'icon-hipster-pool',  0xe416 => 'icon-hipster-portrait',  0xe63c => 'icon-hipster-power',  0xe336 => 'icon-hipster-power_input',  0xe91e => 'icon-hipster-pregnant_woman',
					0xe645 => 'icon-hipster-priority_high',  0xe80b => 'icon-hipster-public',  0xe255 => 'icon-hipster-publish',  0xe560 => 'icon-hipster-rate_review',  0xe8b0 => 'icon-hipster-receipt',  0xe91f => 'icon-hipster-record_voice_over',
					0xe5d5 => 'icon-hipster-refresh',  0xe15d => 'icon-hipster-remove_circle_outline',  0xe8f4 => 'icon-hipster-visibility',  0xe8fe => 'icon-hipster-reorder',  0xe56c => 'icon-hipster-restaurant',  0xe929 => 'icon-hipster-restore_page',
					0xeb49 => 'icon-hipster-room_service',  0xe920 => 'icon-hipster-rounded_corner',  0xe328 => 'icon-hipster-router',  0xe921 => 'icon-hipster-rowing',  0xe642 => 'icon-hipster-rv_hookup',  0xe562 => 'icon-hipster-satellite',
					0xe329 => 'icon-hipster-scanner',  0xe80c => 'icon-hipster-school',  0xe1c2 => 'icon-hipster-sd_storage',  0xe32a => 'icon-hipster-security',  0xe163 => 'icon-hipster-send',  0xe811 => 'icon-hipster-sentiment_dissatisfied',
					0xe813 => 'icon-hipster-sentiment_satisfied',  0xe814 => 'icon-hipster-sentiment_very_dissatisfied',  0xe815 => 'icon-hipster-sentiment_very_satisfied',  0xe8c9 => 'icon-hipster-shop',  0xe8ca => 'icon-hipster-shop_two',  0xe8cb => 'icon-hipster-shopping_basket',
					0xe261 => 'icon-hipster-short_text',  0xe1c8 => 'icon-hipster-signal_cellular_4_bar',  0xe1cd => 'icon-hipster-signal_cellular_connected_no_internet_4_bar',  0xe1cf => 'icon-hipster-signal_cellular_null',  0xe1d0 => 'icon-hipster-signal_cellular_off',  0xe1d8 => 'icon-hipster-signal_wifi_4_bar',
					0xe1d9 => 'icon-hipster-signal_wifi_4_bar_lock',  0xe1da => 'icon-hipster-signal_wifi_off',  0xe41b => 'icon-hipster-slideshow',  0xeb4a => 'icon-hipster-smoke_free',  0xeb4b => 'icon-hipster-smoking_rooms',  0xeb4c => 'icon-hipster-spa',
					0xe256 => 'icon-hipster-space_bar',  0xe32d => 'icon-hipster-speaker',  0xe8cd => 'icon-hipster-speaker_notes',  0xe92a => 'icon-hipster-speaker_notes_off',  0xe8ce => 'icon-hipster-spellcheck',  0xe8d0 => 'icon-hipster-stars',
					0xe1db => 'icon-hipster-storage',  0xe563 => 'icon-hipster-store_mall_directory',  0xe41c => 'icon-hipster-straighten',  0xe56e => 'icon-hipster-streetview',  0xe41d => 'icon-hipster-style',  0xe8d2 => 'icon-hipster-subject',
					0xe56f => 'icon-hipster-subway',  0xe8d3 => 'icon-hipster-supervisor_account',  0xe8d4 => 'icon-hipster-swap_horiz',  0xe8d5 => 'icon-hipster-swap_vert',  0xe8d6 => 'icon-hipster-swap_vertical_circle',  0xe41e => 'icon-hipster-switch_camera',
					0xe628 => 'icon-hipster-sync_disabled',  0xe629 => 'icon-hipster-sync_problem',  0xe62a => 'icon-hipster-system_update',  0xe8d7 => 'icon-hipster-system_update_alt',  0xe8d8 => 'icon-hipster-tab',  0xe8d9 => 'icon-hipster-tab_unselected',
					0xe262 => 'icon-hipster-text_fields',  0xe421 => 'icon-hipster-texture',  0xe422 => 'icon-hipster-timelapse',  0xe922 => 'icon-hipster-timeline',  0xe425 => 'icon-hipster-timer',  0xe423 => 'icon-hipster-timer_10',
					0xe424 => 'icon-hipster-timer_3',  0xe426 => 'icon-hipster-timer_off',  0xe264 => 'icon-hipster-title',  0xe8de => 'icon-hipster-toc',  0xe8df => 'icon-hipster-today',  0xe8e0 => 'icon-hipster-toll',
					0xe427 => 'icon-hipster-tonality',  0xe913 => 'icon-hipster-touch_app',  0xe332 => 'icon-hipster-toys',  0xe8e1 => 'icon-hipster-track_changes',  0xe565 => 'icon-hipster-traffic',  0xe570 => 'icon-hipster-train',
					0xe571 => 'icon-hipster-tram',  0xe572 => 'icon-hipster-transfer_within_a_station',  0xe428 => 'icon-hipster-transform',  0xe8e2 => 'icon-hipster-translate',  0xe8e3 => 'icon-hipster-trending_down',  0xe8e4 => 'icon-hipster-trending_flat',
					0xe8e5 => 'icon-hipster-trending_up',  0xe429 => 'icon-hipster-tune',  0xe333 => 'icon-hipster-tv',  0xe5d6 => 'icon-hipster-unfold_less',  0xe5d7 => 'icon-hipster-unfold_more',  0xe923 => 'icon-hipster-update',
					0xe1e0 => 'icon-hipster-usb',  0xe62d => 'icon-hipster-vibration',  0xe04c => 'icon-hipster-videocam_off',  0xe338 => 'icon-hipster-videogame_asset',  0xe8e9 => 'icon-hipster-view_agenda',  0xe8ea => 'icon-hipster-view_array',
					0xe8eb => 'icon-hipster-view_carousel',  0xe42a => 'icon-hipster-view_comfy',  0xe42b => 'icon-hipster-view_compact',  0xe8ed => 'icon-hipster-view_day',  0xe8ee => 'icon-hipster-view_headline',  0xe8f0 => 'icon-hipster-view_module',
					0xe8f1 => 'icon-hipster-view_quilt',  0xe8f2 => 'icon-hipster-view_stream',  0xe8f3 => 'icon-hipster-view_week',  0xe435 => 'icon-hipster-vignette',  0xe8f5 => 'icon-hipster-visibility_off',  0xe62e => 'icon-hipster-voice_chat',
					0xe42c => 'icon-hipster-wb_auto',  0xe42e => 'icon-hipster-wb_incandescent',  0xe436 => 'icon-hipster-wb_iridescent',  0xe430 => 'icon-hipster-wb_sunny',  0xe63d => 'icon-hipster-wc',  0xe80e => 'icon-hipster-whatshot',
					0xe63e => 'icon-hipster-wifi',  0xe1e2 => 'icon-hipster-wifi_tethering',  0xe25b => 'icon-hipster-wrap_text',  0xe8fa => 'icon-hipster-youtube_searched_for',  0xe8ff => 'icon-hipster-zoom_in',  0xe901 => 'icon-hipster-zoom_out',

				),
				'tags' => array(
					'',
				),
			),
		);
		return $icomoon_icons;
	}

	/**
	 * Edit existing taxonomy.
	 *
	 * @since 1.0
	 */
	public function edit_taxonomy_icon( $term ) {

		// Get the term id
		$term_id = $term->slug;

		// Get the icons
		$term_meta = get_option( $this->taxonomy_font_icons_option );

		if ( ! empty( $term_meta ) ) {
			// Get the icon of the taxonomy
			$term_icon = ( isset( $term_meta[ $term_id] ) ) ? $term_meta[$term_id] : '';
		}else{
			$term_icon = '';
		}
		?>

		<tr class="form-field">
			<th scope="row">
				<label for="icons"><?php _e( 'Icon','taxonomy_icon_font' ); ?></label>
				<td>

				<select data-bv-notempty="true" data-bv-notempty-message="<?php _e( 'You must pick a font','taxonomy_icon_font' ); ?>" name="icon" id="icons" class="form-control by_class font-icon-picker">
				    <option value="" selected="selected"><?php _e( '--Please Select--.','taxonomy_icon_font' ); ?></option>
				    <?php echo imii_generate_select_options( $this->icomoon_icons(), 'class', $term_icon ); ?>
				</select>

				<p class="description"><?php _e( 'Select an icon that will be connected to the taxonomy.','taxonomy_icon_font' ); ?></p>

				</td>
			</th>
		</tr>

		<?php
	}

	/**
	 * Save taxonomy icon.
	 *
	 * Saves the taxonomy->icon connection to wp_options.
	 *
	 * @since 1.0
	 */
	public function save_taxonomy_icon( $term_id ) {

		$taxonomy = get_taxonomy( $_REQUEST['taxonomy'] );

		if ( isset( $_POST['icon'] ) ) {

			// Get the selected icon for the taxonomy
			$selected_icon = esc_attr( $_POST['icon'] );

			// Get the icons
			$tfi_icons = get_option( $this->taxonomy_font_icons_option );

			// Update the icons array with the key value pair
			$term = get_term( $term_id, $taxonomy->name );
			$term_slug = $term->slug;

			$tfi_icons[ $term_slug ] = $selected_icon;

			// Update the option
			update_option( $this->taxonomy_font_icons_option, $tfi_icons );
		}
	}

	/**
	 * Enqueue stylesheets.
	 *
	 * Enqueues the stylesheet which has all the icons.
	 *
	 * @since 1.0
	 */
	public function enqueue_stylesheets() {
		wp_enqueue_style( 'taxonomy-font-icons-stylesheet', $this->stylesheet );

		if(is_admin()){

			$styles = array(
				'jquery.fonticonpicker.min',
				'themes/grey-theme/jquery.fonticonpicker.grey.min',
				'themes/bootstrap-theme/jquery.fonticonpicker.bootstrap.min',
				'iconmoon',
				'taxonomy-font-icons',
			);
			foreach($styles as $style){
				wp_enqueue_style( 'taxonomy-icon-'.$style, plugin_dir_url( __FILE__ ) . '/assets/'.$style.'.css');
			}

			$scripts = array(
				'jquery.fonticonpicker.min',
				'taxonomy-font-icon',
			);
			foreach($scripts as $script){
				wp_enqueue_script( 'taxonomy-icon-'.$script, plugin_dir_url( __FILE__ ) . '/assets/'.$script.'.js', false, '1.0.0', true );
			}
	    }
	}
}